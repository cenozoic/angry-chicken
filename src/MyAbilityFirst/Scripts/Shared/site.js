﻿function AJAXPost(url, data, successCallback, failCallback) {
	$.ajax({
		data: JSON.stringify(data),
		contentType: "application/json; charset=utf-8",
		type: 'POST',
		cache: false,
		url: url,
		timeout: 5000,
		success: function (datares) {
			for (var i = 0; i < successCallback.length; i++) {
				successCallback[i](datares);
			}
		}
	}).fail(function (errorThrown) {
		failCallback(errorThrown);
	});
}

function AJAXGet(url, data, successCallback, failCallback) {
	$.ajax({
		data: data,
		type: 'GET',
		cache: false,
		url: url,
		timeout: 5000,
		success: function (datares) {
			for (var i = 0; i < successCallback.length; i++) {
				successCallback[i](datares);
			}
		}
	}).fail(function (errorThrown) {
		failCallback(errorThrown);
	});
}

function AJAXAttach(url, data, successCallback, failCallback) {
	$.ajax ({
		data: data,
		type: "POST",
		cache: false,
		url: url,
		contentType: false,
		processData: false,
		timeout: 30000,
		success: function (datares) {
			for (var i = 0; i < successCallback.length; i++) {
				successCallback[i](datares);
			}
		}
	}).fail(function (errorThrown) {
		failCallback(errorThrown);
	});
}

function ReplaceElementCallback(replacingElement) {
	return function (datares) {
		$(replacingElement).replaceWith(datares.Html);
	}
}

function ReplaceHtmlCallback(parentElement) {
	return function (datares) {
		$(parentElement).html(datares.Html);
	}
}

function AppendElementCallback(appendingElementName) {
	return function (datares) {
		$(appendingElementName).append(datares.Html);
	}
}

var ajaxDataObj = (
	function () {
		var data;
		var pub = {};

		pub.getData = function () {
			return data;
		};
		pub.setData = function (newData) {
			data = newData;
		};
		pub.flipSelected = function () {
			data.Selected = !data.Selected;
		};
		return pub;
	}()
);

function AJAXPostShortlistButton(url, data, buttonElement, toggleClasses, followUp) {
	if (ajaxDataObj.getData() == null) {
		ajaxDataObj.setData(data);
	}
	ajaxDataObj.flipSelected();

	followUp.unshift(ToggleButtonCallback(buttonElement, toggleClasses))
	$(buttonElement).button('loading').delay(100).queue(function () {
		AJAXPost(url, ajaxDataObj.getData(), followUp, ResetButtonCallback(buttonElement))
	});
};

function ToggleButtonCallback(buttonElement, toggleClasses) {
	return function (datares) {
		$(buttonElement).toggleClass(toggleClasses);
		ResetDequeueButton(buttonElement);
		ajaxDataObj.setData(datares);
	}
}

function ResetButtonCallback(buttonElement) {
	return function (errorThrown) {
		ajaxDataObj.flipSelected();
		ResetDequeueButton(buttonElement);
		WriteErrorToConsole(errorThrown);
	}
}

function ResetDequeueButton(buttonElement) {
	$(buttonElement).button('reset');
	$(buttonElement).dequeue();
}

function WriteErrorToConsole(errorThrown) {
	console.log(errorThrown.statusText);
}

$(function () {

	initPredictiveSearch();
	initCallback()();


	function initCallback() {
		return function (datares) {
			var map;
			if ($("#profilemap")[0] != null) {
				map = getProfileMap(+document.getElementById('HomeLatitude').value, +document.getElementById('HomeLongitude').value, +document.getElementById('LocationCoverageRadius').value);
			}
			if ($("#searchmap")[0] != null) {
				map = getSearchMap(datares.HomeLatitude, datares.HomeLongitude, datares.Latitude, datares.Longitude);
				loadTagsFromCheckbox(map);
				populateMarkers(map)();
				SetupSortingButtonEvents(map);
			}
		}
	}

	// Google maps
	function getProfileMap(homeLat, homeLng, coverageRadius) {
		var map = new google.maps.Map(document.getElementById('profilemap'), {
			center: {
				lat: homeLat,
				lng: homeLng
			},
			mapTypeControl: false,
			streetViewControl: false,
			zoom: 11
		});

		var marker = new google.maps.Marker({
			position: map.center,
			map: map,
			title: 'Click me to search within area'
		});
		marker.setMap(map);

		var circle = new google.maps.Circle({
			strokeColor: '#FF00FF',
			strokeOpacity: 0.8,
			strokeWeight: 2,
			fillColor: '#FFFFFF',
			fillOpacity: 0.30,
			center: marker.position,
			radius: coverageRadius * 1000
		});
		circle.setMap(map);
		return map;
	}

	function getSearchMap(homeLat, homeLng, centerLat, centerLng) {
		var map = new google.maps.Map(document.getElementById('searchmap'), {
			center: { lat: centerLat, lng: centerLng },
			mapTypeControl: false,
			streetViewControl: false,
			fullscreenControl: false,
			zoomControl: true,
			zoomControlOptions: {
				position: google.maps.ControlPosition.TOP_RIGHT
			},
			zoom: 13
		});
		SearchResultsMarkers = [];

		var homeMarker = new google.maps.Marker({
			position: new google.maps.LatLng(homeLat, homeLng),
			map: map,
			icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
		});
		homeMarker.setMap(map);

		google.maps.event.addListener(map, 'idle', function () {
			if ($('input[name=updatesearchwhenmapmove]')[0].checked) {
				searchCareWorkerAndUpdateMap(1, map, [ReplaceHtmlCallback('#search-results-content'), populateMarkers(map), PagingCallback(map), UpdateSearchResultsCountCallback()]);
			}
		});
		return map;
	}

	function searchCareWorkerAndUpdateMap(pageNumber, map, callbacks) {
		AJAXPost(
			'/Search/SearchCareWorkerPaged',
			{
				MinLong: map.getBounds().getSouthWest().lng(),
				MinLat: map.getBounds().getNorthEast().lat(),
				MaxLong: map.getBounds().getNorthEast().lng(),
				MaxLat: map.getBounds().getSouthWest().lat(),
				PostedSubcategoryIDs: JSON.parse(getCheckBoxListSelectedValues()),
				PageNumber: pageNumber,
				SortOption: $('#SortOption .active')[0].getAttribute('data-sortvalue')
			},
			callbacks,
			WriteErrorToConsole
		)
	}

	function getCheckBoxListSelectedValues() {
		var chkBox = document.querySelectorAll("input[name='PostedSubcategoryIDs'][type='checkbox']");
		var checkeditems = [];
		for (var i = 0; i < chkBox.length; i++) {
			if (chkBox[i].checked) {
				checkeditems.push(chkBox[i].value);
			}
		}
		return JSON.stringify(checkeditems).replace(/['"]+/g, '');
	}

	function populateMarkers(map) {
		return function (datares) {
			// deallocate existing markers
			if (SearchResultsMarkers != null) {
				for (var i = 0; i < SearchResultsMarkers.length; i++) {
					SearchResultsMarkers[i].infoWindow.setMap(null);
					SearchResultsMarkers[i].infoWindow = null;
					SearchResultsMarkers[i].setMap(null);
					delete SearchResultsMarkers[i];
				};
				SearchResultsMarkers.length = 0;
			}

			// create new markers
			var newMarkers = getMarkerInfoFromDOM("SearchResults", "div");
			newMarkers.forEach(function (item) {
				var content = item[2];
				var infoWindow = new google.maps.InfoWindow({
					content: content
				});

				var newMarker = new google.maps.Marker({
					position: new google.maps.LatLng(item[0], item[1]),
					map: map,
					infoWindow: infoWindow
				});

				newMarker.addListener('mouseover', function () {
					infoWindow.open(map, this);
				});

				newMarker.addListener('mouseout', function () {
					infoWindow.close();
				});

				SearchResultsMarkers.push(newMarker);
			});
		}
	}

	function getMarkerInfoFromDOM(name, displayHtmlType) {
		// Returns an array of [Lat,Long,DisplayingHTML]
		var allLat = document.querySelectorAll("input[name^='" + name + "['][name$='].Latitude']");
		var allLng = document.querySelectorAll("input[name^='" + name + "['][name$='].Longitude']");
		var allHtml = document.querySelectorAll(displayHtmlType + "[name^='" + name + "['][name$='].Html']");
		var count = allLat.length;

		var res = [];
		for (var i = 0; i < count; i++) {
			var item = [];
			item.push(allLat[i].value);
			item.push(allLng[i].value);
			item.push(allHtml[i].innerHTML);
			res.push(item);
		}
		return res;
	}
	
	// Tags input
	function loadTagsFromCheckbox(map) {
		$('.category-tag-area').unbind();
		$('.category-tabs').unbind();
		$('.category-tag-area').tagsinput({
			allowDuplicates: false,
			itemValue: 'id',
			itemText: 'text'
		});

		// initalize tags on page load
		$.each($("input[type='checkbox'][checked='checked']"), function (index, value) {
			$('.category-tag-area').tagsinput('add', { id: +value.value, text: value.parentNode.childNodes[1].innerText });
		});

		// update tags on checkbox change
		$('input[type=checkbox]').change(function () {
			if ($(".category-tag-area").length > 0 && this.value != null && this.parentNode.childNodes[1].innerText != null) {
				$(".category-tag-area").tagsinput(this.checked ? 'add' : 'remove', { id: +this.value, text: this.parentNode.childNodes[1].innerText });
			}
		});

		// update checkbox on tags change
		$(".category-tag-area").on('itemAdded', function (event) {
			$("input[type='checkbox'][value='" + +event.item.id + "']")[0].checked = true;
			searchCareWorkerAndUpdateMap(1, map, [ReplaceHtmlCallback('#search-results-content'), populateMarkers(map), PagingCallback(map), UpdateSearchResultsCountCallback()]);
		});
		$(".category-tag-area").on('itemRemoved', function (event) {
			$("input[type='checkbox'][value='" + +event.item.id + "']")[0].checked = false;
			searchCareWorkerAndUpdateMap(1, map, [ReplaceHtmlCallback('#search-results-content'), populateMarkers(map), PagingCallback(map), UpdateSearchResultsCountCallback()]);
		});

		// category menu animation
		$('.category-tabs').click(function () {
			$(this).find('i').toggleClass('fa-angle-up fa-angle-down');
		});
	}

	// Typeahead and bloodhound
	function initPredictiveSearch() {
		$('#predictive-search-bar').unbind();
		var nameSource = new Bloodhound({
			limit: 5,
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			remote: {
				url: '/search/SearchCareWorkerNameJson',
				rateLimitWait: 300,
				replace: function (url) {
					return url + '?careWorkerName=' + $('#predictive-search-bar')[0].value;
				},
				filter: function (carers) {
					return $.map(carers, function (item) {
						return {
							value: item.FirstName + ' ' + item.LastName,
							userID: item.CareWorkerID,
							pictureURL: item.PictureURL,
							source: "Name-Dataset"
						};
					});
				}
			}
		});

		var locationSource = new Bloodhound({
			limit: 5,
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			remote: {
				url: '/search/SearchLocationJson',
				rateLimitWait: 300,
				replace: function (url) {
					return url + '?locationName=' + $('#predictive-search-bar')[0].value;
				},
				filter: function (loc) {
					return $.map(loc, function (item) {
						return {
							value: item.description,
							place_id: item.place_id,
							source: "Location-Dataset"
						};
					});
				}
			}
		});

		function GetAutocompleteCategories() {
			var allSubcategoryCheckboxes = document.querySelectorAll("input[name='PostedSubcategoryIDs'][type='checkbox']");
			var allSubcategoryNames = [];
			var data = new Object();
			for (var i = 0; i < allSubcategoryCheckboxes.length; i++) {
				allSubcategoryNames.push({
					label: allSubcategoryCheckboxes[i].parentNode.childNodes[1].innerText,
					category: allSubcategoryCheckboxes[i].parentNode.parentNode.parentNode.parentNode.getAttribute('label'),
					subcategoryID: allSubcategoryCheckboxes[i].value
				});
			}
			return allSubcategoryNames;
		}

		var subCategoryList = GetAutocompleteCategories();
		var subCategorySource;
		if (subCategoryList.length > 0) { // on search page, get directly from DOM
			subCategorySource = new Bloodhound({
				limit: 5,
				datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: $.map(subCategoryList, function (item) {
					return {
						value: item.label,
						category: item.category,
						subcategoryID: item.subcategoryID,
						source: "Subcategory-Dataset"
					};
				})
			});
		} else { // not on search page, get suggestions from server
			subCategorySource = new Bloodhound({
				limit: 5,
				datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				remote: {
					url: '/search/SearchSubcategoryJson',
					rateLimitWait: 300,
					replace: function (url) {
						return url + '?subcategoryName=' + $('#predictive-search-bar')[0].value;
					},
					filter: function (subcategories) {
						return $.map(subcategories, function (item) {
							return {
								value: item.SubcategoryName,
								category: item.CategoryName,
								subcategoryID: item.SubcategoryID,
								source: "Subcategory-Dataset"
							};
						});
					}
				}
			});
		}

		nameSource.initialize();
		locationSource.initialize();
		subCategorySource.initialize();

		$('#predictive-search-bar').typeahead(
			{
				hint: true,
				highlight: true,
				minLength: 3
			},
			{
				name: 'Name-Dataset',
				displayKey: 'value',
				source: nameSource.ttAdapter(),
				templates: {
					header: '<h>CARERS</h>',
					suggestion: function (data) {
						return '<p><img src="' + data.pictureURL + '" class="media-object img-circle" style="width:30px"></span>  ' + data.value + '</p>';
					}
				}
			},
			{
				name: "Location-Dataset",
				displayKey: 'value',
				source: locationSource.ttAdapter(),
				templates: {
					header: '<h>LOCATION</h><img class="pull-right" src="https://developers.google.com/places/documentation/images/powered-by-google-on-white.png"/>',
					suggestion: function (data) {
						return '<p><span class="fa fa-map-marker"></span>  ' + data.value + '</p>';
					}

				}
			},
			{
				name: "Subcategory-Dataset",
				displayKey: 'value',
				source: subCategorySource.ttAdapter(),
				templates: {
					header: '<h>CARER PREFERENCES</h1>',
					suggestion: function (data) {
						return '<p><span class="fa fa-tag"></span> (' + data.category + ') ' + data.value + '</p>';
					}
				}
			}
		).bind("typeahead:selected", BindTypeaheadEvents());
	};

	function BindTypeaheadEvents() {
		return function (obj, datum) {
			if (datum.source == "Name-Dataset") {
				$('#predictive-search-bar').typeahead('val', '');
				$(location).attr('href', "/profile/PublicProfile/" + datum.userID);
			}
			if (datum.source == "Location-Dataset") {
				$('#predictive-search-bar').typeahead('val', '');
				// load search page with suburb as center
				AJAXPost(
					'/Search/SearchCareWorker',
					{
						PlaceID: datum.place_id,
						SortOption: jQuery("#SortOption option:selected").val()
					},
					[ReplaceElementCallback('#main'), initCallback()],
					WriteErrorToConsole
				);
			}
			if (datum.source == "Subcategory-Dataset") {
				$('#predictive-search-bar').typeahead('val', '');

				// already on search page, add tag and update search
				if ($(".category-tag-area").length > 0) {
					$(".category-tag-area").tagsinput('add', { id: +datum.subcategoryID, text: datum.value });
				} else { // not on search page, load search page with subcategory ID
					AJAXPost(
						'/Search/SearchCareWorker',
						{
							PostedSubcategoryIDs: [datum.subcategoryID],
							SortOption: jQuery("#SortOption option:selected").val()
						},
						[ReplaceElementCallback('#main'), initCallback()],
						WriteErrorToConsole
					);
				}
			}
		}
	};

	// Search results paging
	function PagingCallback(map) {
		return function (datares) {
			$('#search-results-content').unbind();
			$("#search-results-content").on('scroll', function () {
				if (this.clientHeight + Math.round(this.scrollTop) >= this.scrollHeight && datares.PageNumber < datares.PageCount) { // if scrolled to bottom and not end of page
					searchCareWorkerAndUpdateMap(datares.PageNumber + 1, map, [AppendElementCallback('#search-results-content'), populateMarkers(map), PagingCallback(map)]);
				}
			});
		};
	}
	
	function UpdateSearchResultsCountCallback() {
		return function (datares) {
			if (datares.Success) {
				$('.search-match-count-p').contents()[2].textContent = " " + (datares.ResultsCount > 1 ? datares.ResultsCount + " Matches" : (datares.ResultsCount == 1 ? "1 Match" : "No Match"));
				$('#search-results-content')[0].scrollTop = 0;
			}
		};
	};

	function SetupSortingButtonEvents(map) {
		$('#SortOption').unbind();
		$("#SortOption").click(function (event) {
			setTimeout(function () {
				searchCareWorkerAndUpdateMap(1, map, [ReplaceHtmlCallback('#search-results-content'), populateMarkers(map), PagingCallback(map), UpdateSearchResultsCountCallback()]);
			}, 0);
		});
	};

});

// Chatroom
$(function () {

	// Declare a proxy to reference the hub. 
	var hub = $.connection.signalRHub;
	// Start the connection.
	$.connection.hub.start().done(function () {
	});

	// Message endpoint
	hub.client.sendMessage = function (message, roomID) {
		// Add the message to the page. 
		$('#chatcontainer[data-roomID=\'' + roomID + '\'').children('#chatcontent').children('#chatmessages').append(message);
		$('#chatcontainer[data-roomID=\'' + roomID + '\'').children('#chattitle').addClass('new-message-hightlight');
		$('#chatmessages[data-roomID=\'' + roomID + '\'').animate({ scrollTop: $('#chatmessages[data-roomID=\'' + roomID + '\'')[0].scrollHeight }, 600);
	};

	// Typing endpoints
	hub.client.sendStartTypingNotification = function (message, roomID) {
		var notificationText = $('#chatcontainer[data-roomID=\'' + roomID + '\'').children('#chatcontent').children('#chattypingnotification').text();
		$('#chatcontainer[data-roomID=\'' + roomID + '\'').children('#chatcontent').children('#chattypingnotification').text(notificationText + ' ' + message);
	};
	hub.client.sendStopTypingNotification = function (message, roomID) {
		var notification = $('#chatcontainer[data-roomID=\'' + roomID + '\'').children('#chatcontent').children('#chattypingnotification').text().replace(message, '').trim();
		$('#chatcontainer[data-roomID=\'' + roomID + '\'').children('#chatcontent').children('#chattypingnotification').html(notification);
	};

	// Notification endpoint
	hub.client.sendAlert = function (html, notificationID) {
		var notif = $('#NotificationContainer' + notificationID);
		// if notification not exists or already read, increment counter		
		if (notif.length == 0 || notif.children('#NotificationBody')[0].dataset.read == 'True') {
			$('#notificationcounter').text(+$('#notificationcounter').text() + 1);
		}
		notif.remove();
		$('#Notifications').append(html);
	}

	// Notification redirect events
	$(document).on('click', '#NotificationBody', function () {
		// Chat notifications
		if ($(this).children('#chatlink').length == 1) {
			getChatRoom($(this).children('#chatlink')[0].dataset.roomid);
		}
		if (this.dataset.read == "False") { // if not read
			AJAXPost('/chatroom/ChatNotificationRead/',
			{
				notificationID: this.dataset.notificationid
			},
			[UpdateNotificationAsReadCallback()], WriteErrorToConsole);
		}
	});

	// Notification close events
	$(document).on('click', '#NotificationClose', function () {
		AJAXPost('/chatroom/ChatNotificationClosed/',
		{
			notificationID: this.dataset.notificationid
		},
		[UpdateNotificationAsClosedCallback(this.dataset.notificationid)], WriteErrorToConsole);
	});

	// Get Private Chatroom and load on page event
	$(document).on('click', '#privatechatlink', function () {
		if ($('#chatcontainer[data-recipientID=\'' + this.dataset.recipientid + '\'').length <= 0) {
			AJAXGet('/chatroom/GetPrivateChatRoomJson/',
			{
				recipientID: this.dataset.recipientid,
			},
			[AttachChatRoomCallback()], WriteErrorToConsole);
		}
	});

	// Get Chatroom and load on page
	function getChatRoom(roomID) {
		if ($('#chatcontainer[data-roomID=\'' + roomID + '\'').length <= 0) {
			AJAXGet('/chatroom/GetChatRoomJson/',
			{
				roomID: roomID,
			},
			[AttachChatRoomCallback()], WriteErrorToConsole);
		} else { // room already exists, maximize / focus
			if ($('#chatminimize[data-roomID=\'' + roomID + '\'').is(":visible")) {
				maximizeChat(roomID, false);
			}
			$('#chatcontainer[data-roomID=\'' + roomID + '\'').children('#chattitle').removeClass('new-message-hightlight');
		}
	}

	// Send Message
	$(document).on('click', '#sendmessage', function () {
		AJAXPost('/chatroom/postmessage/',
		{
			roomID: this.dataset.roomid,
			content: JSON.stringify($('#Message[data-roomID=\'' + this.dataset.roomid + '\'').val()).replace(/['"]+/g, '')
		},
		[FocusToLastMessageCallback(this.dataset.roomid)], WriteErrorToConsole);

		// message sent, finish typing
		delete timeouts[this.dataset.roomid];
		AJAXGet('/chatroom/StopTypingNotification/',
		{
			roomID: this.dataset.roomid
		},
		[], WriteErrorToConsole);
	});

	// Scrolling event for paged results
	document.addEventListener('scroll', function (event) {
		// if scrolled to top, retrieve more messages
		if (event.target.id === 'chatmessages' && $('#' + event.target.id).scrollTop() == 0) {
			PrependMessages(event.target);
		}
	}, true);

	// load older messages
	function PrependMessages(chatmessages) {
		if (+chatmessages.dataset.pagenumber < +chatmessages.dataset.pagecount) {
			AJAXGet('/chatroom/GetMessages/',
			{
				roomID: +chatmessages.dataset.roomid,
				pageNumber: +chatmessages.dataset.pagenumber + 1
			},
			[PrependMessagesCallback(+chatmessages.dataset.roomid)], WriteErrorToConsole);
		}
	}

	// broadcast typing notification
	var timeouts = {};
	$(document).on('keyup', '#Message', function (e) {
		var roomid = this.dataset.roomid;
		if (roomid in timeouts) {
			clearTimeout(timeouts[roomid]); // reset timer, still typing
		}
		else {
			AJAXGet('/chatroom/StartTypingNotification/',
			{
				roomID: roomid
			},
			[], WriteErrorToConsole);
		}
		timeouts[roomid] = setTimeout(function () {
			AJAXGet('/chatroom/StopTypingNotification/',
			{
				roomID: roomid
			},
			[], WriteErrorToConsole);
			delete timeouts[roomid];
			}, 3000);
	});

	function AttachChatRoomCallback() {
		return function (datares) {
			if (datares.Success) {
				var minimizedHeight = 50;
				$('body').append(datares.Html);
				var roomID = $('<div/>').html(datares.Html).contents()[0].dataset.roomid;
				$('#chatcontainer[data-roomID=\'' + roomID + '\'').css({ 
					position: "fixed",
					marginLeft: 0, marginTop: 0,
					bottom: 0, right: ($('[id^=chatcontainer][style*="height: ' + minimizedHeight + 'px"]').length) * $('#chatcontainer[data-roomID=\'' + roomID + '\'').width()
				});

				// scroll to bottom
				$('#chatmessages[data-roomID=\'' + roomID + '\'')[0].scrollTop = $('#chatmessages[data-roomID=\'' + roomID + '\'')[0].scrollHeight;
				$('#chatmaximize[data-roomID=\'' + roomID + '\'').hide();

				// make chatbox draggable
				$(".draggable").draggable({
					containment: 'parent',
					stack: '.draggable',
					scroll: true
				});

				// turning off new message highlight after clicking
				$('#chatcontent[data-roomID=\'' + roomID + '\'').click(function () {
					var room = $('#chatcontainer[data-roomID=\'' + roomID + '\'');
					// set as read if chat room maximized and new message hightlight active
					if (room.height() > minimizedHeight && room.children('#chattitle').hasClass('new-message-hightlight')) {
						AJAXPost('/chatroom/ChatNotificationReadViaRoom/',
						{
							roomID: roomID
						},
						[UpdateNotificationAsReadCallback()], WriteErrorToConsole);
						room.children('#chattitle').removeClass('new-message-hightlight');
					}
				});

				// minimize chat
				$('#chatminimize[data-roomID=\'' + roomID + '\'').click(function (e) {
					minimizeChat(roomID, minimizedHeight);
				});

				// expand chat
				$('#chatmaximize[data-roomID=\'' + roomID + '\'').click(function (e) {
					maximizeChat(roomID, true);
				})

				// close chat
				$('#chatclose[data-roomID=\'' + roomID + '\'').click(function () {
					$('#chatcontainer[data-roomID=\'' + roomID + '\'').remove();
				});
			}
		}
	}

	function minimizeChat(roomID, minimizedHeight) {
		$('#chatminimize[data-roomID=\'' + roomID + '\'').hide();
		$('#chatmaximize[data-roomID=\'' + roomID + '\'').show();
		$('#chatcontent[data-roomID=\'' + roomID + '\'').slideUp('fast');
		$('#chatcontainer[data-roomID=\'' + roomID + '\'').draggable({ revert: true }).css({
			height: minimizedHeight,
			bottom: 0, top: 'auto', right: ($('[id^=chatcontainer][style*="height: ' + minimizedHeight + 'px"]').length) * $('#chatcontainer[data-roomID=\'' + roomID + '\'').width(), left: 'auto',
		});
	}

	function maximizeChat(roomID, dock) {
		$('#chatmaximize[data-roomID=\'' + roomID + '\'').hide();
		$('#chatminimize[data-roomID=\'' + roomID + '\'').show();
		$('#chatcontent[data-roomID=\'' + roomID + '\'').slideDown('fast');
		$('#chatcontainer[data-roomID=\'' + roomID + '\'').draggable({ revert: false }).css({
			height: 600,
		});
		if (dock) {
			$('#chatcontainer[data-roomID=\'' + roomID + '\'').draggable({ revert: false }).css({
				bottom: 0, top: 'auto'
			});
		}
	}

	function PrependMessagesCallback(roomID) {
		return function (datares) {
			if (datares.Success) {
				$('#chatmessages[data-roomID=\'' + roomID + '\'').prepend(datares.Html);
				$('#chatmessages[data-roomID=\'' + roomID + '\'')[0].dataset.pagenumber++;
			}
		}
	}

	function FocusToLastMessageCallback(roomID) {
		return function (datares) {
			// Clear text box and scroll to bottom
			if (datares.Success) {
				$('#Message[data-roomID=\'' + roomID + '\'').val('').focus();
				$('#chatmessages[data-roomID=\'' + roomID + '\'').append(datares.Html);
				$('#chatmessages[data-roomID=\'' + roomID + '\'').animate({ scrollTop: $('#chatmessages[data-roomID=\'' + roomID + '\'')[0].scrollHeight }, 1000);
			}
		}
	}

	function UpdateNotificationAsReadCallback() {
		return function (datares) {
			// Replace current element and refresh counter
			if (datares.Success) {
				var notificationCounter = +$('#notificationcounter').text();
				$('#notificationcounter').text(notificationCounter - 1 > 0 ? notificationCounter - 1 : "");
				$('#NotificationContainer' + $(datares.Html).children()[0].dataset.notificationid).remove();
				$('#Notifications').append(datares.Html);
			}
		}
	}

	function UpdateNotificationAsClosedCallback(notificationID){
		return function (datares) {
			if (datares.Success) {
				if ($('#NotificationContainer' + notificationID).length == 1) {
					if ($('#NotificationContainer' + notificationID).children('#NotificationBody')[0].dataset.read == 'False') { // if removing non-read notification, decrease counter
						var notificationCounter = +$('#notificationcounter').text();
						$('#notificationcounter').text(notificationCounter - 1 > 0 ? notificationCounter - 1 : "");
					}
					$('#NotificationContainer' + notificationID).remove();
				}
			}
		}
	}

});

// Uploads
$(function () {

	$("#profilephoto-upload-input").change(function (e) {
		e.preventDefault();
		var formData = new FormData(this);
		var file = this.files[0];
		formData.append('file', file);
		AJAXAttach(
			'/Attachment/CreateProfilePhoto',
			formData,
			[AppendElementCallback('#ImageUploadContainer')], WriteErrorToConsole
		);
		this.value = null;
	});

});