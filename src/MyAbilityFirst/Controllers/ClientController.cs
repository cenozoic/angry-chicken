﻿using AutoMapper;
using System.Collections.Generic;
using System.Web.Mvc;
using Cenozoic.Models;
using Cenozoic.Services;
using MyAbilityFirst.Domain;
using MyAbilityFirst.Services.ClientFunctions;
using MyAbilityFirst.Services.Common;

namespace MyAbilityFirst.Controllers
{
	[Authorize(Roles = "Client")]
	public class ClientController : Controller
	{

		#region Fields

		private readonly IMapper _mapper;
		private readonly IClientService _clientServices;
		private readonly IPresentationService _categoryServices;
		private readonly IUserService _userServices;

		#endregion

		#region Ctor

		public ClientController(IMapper mapper, IClientService clientServices, IPresentationService categoryServices, IUserService userServices)
		{
			this._mapper = mapper;
			this._clientServices = clientServices;
			this._categoryServices = categoryServices;
			this._userServices = userServices;
		}

		#endregion

		#region Actions

		[HttpGet, Route("client/profile/edit")]
		public ActionResult EditProfile()
		{
			ClientDetailsViewModel vm = mapClientToVM();
			return View(vm);
		}

		[HttpPost, Route("client/profile/edit")]
		[ValidateAntiForgeryToken]
		public ActionResult EditProfile(ClientDetailsViewModel vm)
		{
			if (ModelState.IsValid)
			{
				Client updatedClient = mapVMToClient(vm);
				_clientServices.UpdateClient(updatedClient);

				_userServices.ReplaceAllUserSubCategories(updatedClient.ID, vm.PostedSubCategoryIDs, new List<UserSubcategory>());
			}
			return RedirectToAction("MyAccount/");
		}

		[HttpGet, Route("client/myaccount")]
		public ActionResult MyAccount()
		{
			ClientDetailsViewModel vm = mapClientToVM();
			return View(vm);
		}

		#endregion

		#region Actions-rating

		[HttpGet]
		public ActionResult Rating(int bookingID)
		{
			var vm = new RatingViewModel();
			return View(vm);
		}

		[HttpPost]
		public ActionResult Rating(RatingViewModel vm)
		{
			var client = this.GetLoggedInUser();
			this._clientServices.AddRating(client.ID, vm);
			return RedirectToAction("RatingDetails", new { vm.BookingID });
		}

		[HttpGet]
		public ActionResult UpdateRating(int bookingID)
		{
			var client = this.GetLoggedInUser();
			var vm = this._clientServices.GetRatingVM(client.ID, bookingID);
			vm.OldOverallScore = vm.OverallScore;
			return View(vm);
		}

		[HttpPost]
		public ActionResult UpdateRating(RatingViewModel vm)
		{
			var client = this.GetLoggedInUser();
			this._clientServices.UpdateRating(client.ID, vm.OldOverallScore, vm);
			return RedirectToAction("RatingDetails", new { vm.BookingID });
		}

		[HttpGet]
		public ActionResult RatingDetails(int bookingID)
		{
			var client = this.GetLoggedInUser();
			var vm = this._clientServices.GetRatingVM(client.ID, bookingID);
			return View(vm);
		}

		#endregion

		#region Helpers

		private ClientDetailsViewModel mapClientToVM()
		{
			ClientDetailsViewModel vm = new ClientDetailsViewModel();
			var client = _clientServices.RetrieveClient(this.GetLoggedInUser().ID);
			return _mapper.Map<Client, ClientDetailsViewModel>(client);
		}

		private Client mapVMToClient(ClientDetailsViewModel vm)
		{
			return _mapper.Map(vm, (Client)this.GetLoggedInUser());
		}

		private JsonResult attachmentProcess(string url)
		{
			bool isUploaded = false;
			if (url != null)
			{
				isUploaded = true;
				string message = "100% complete";

				return Json(new
				{
					statusCode = 200,
					status = "File uploaded.",
					file = url,
					isUploaded = isUploaded,
					message = message
				}, "text/html");

			}
			else
			{
				string message = "Error";
				return Json(new
				{
					statusCode = 500,
					status = "Error uploading image.",
					file = string.Empty,
					isUploaded = isUploaded,
					message = message
				}, "text/html");
			}
		}

		#endregion

	}
}
