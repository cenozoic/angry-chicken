﻿using Cenozoic.Models;
using Cenozoic.Infrastructure;
using Cenozoic.Services;
using System.Web;
using System.Web.Mvc;

namespace MyAbilityFirst.Controllers
{
	[Authorize]
	//[AuthorizeForAttachments]
	public class AttachmentController : Controller
	{

		#region Fields

		private readonly IReadEntities _entities;
		private readonly IAttachmentService _attachmentService;

		#endregion

		#region Ctor

		public AttachmentController(IReadEntities entities, IAttachmentService attachmentService)
		{
			this._entities = entities;
			this._attachmentService = attachmentService;
		}

		#endregion

		#region Actions

		[HttpPost]
		public JsonResult CreateProfilePhoto(HttpPostedFileBase File)
		{
			return createAttachment(File, AttachmentType.ProfilePhoto);
		}

		[HttpGet]
		public JsonResult Update(int attachmentID)
		{
			JsonResult res = error("Failed to update");
			var attachment = _attachmentService.RetrieveAttachment(attachmentID);
			HttpPostedFileBase file = Request.Files[0];
			if (file != null && attachment.OwnerUserID == this.GetLoggedInUser().ID)
			{
				attachment = this._attachmentService.UpdateAttachment(attachment.ID, file);
				if (attachment != null)
					res = ok("File uploaded", "<img src='" + attachment.URL + "'/>");
			}
			return setResultsAsAllowGet(ref res);
		}

		[HttpGet]
		public JsonResult Delete(int attachmentID)
		{
			JsonResult res = error("Failed to delete");
			var attachment = _attachmentService.RetrieveAttachment(attachmentID);
			if (attachment.OwnerUserID == this.GetLoggedInUser().ID && this._attachmentService.DeleteAttachment(attachmentID))
				res = ok("File deleted", null);
			return setResultsAsAllowGet(ref res);
		}

		#endregion

		#region Helpers
		
		private JsonResult ok(string message, string html)
		{
			return
				Json(new
				{
					statusCode = 200,
					status = "OK",
					message = message,
					Html = html,
				}, "text/html");
		}

		private JsonResult error(string message)
		{
			return
				Json(new
				{
					statusCode = 500,
					status = "Error",
					message = message
				}, "text/html");
		}

		private JsonResult setResultsAsAllowGet(ref JsonResult result)
		{
			result.ContentEncoding = System.Text.Encoding.UTF8;
			result.ContentType = "application/json";
			result.MaxJsonLength = 100000;
			result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
			result.RecursionLimit = 50;
			return result;
		}

		private JsonResult createAttachment(HttpPostedFileBase file, AttachmentType type)
		{
			JsonResult res = error("Failed to create");
			if (file != null)
			{
				var attachment = this._attachmentService.CreateAttachment(this.GetLoggedInUser().ID, type, file);
				if (attachment != null)
					res = ok("File uploaded", "<img src='" + attachment.URL + "'/>");
			}
			return setResultsAsAllowGet(ref res);
		}

		#endregion

	}
}