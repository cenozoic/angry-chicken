﻿using AutoMapper;
using MyAbilityFirst.Domain;
using MyAbilityFirst.Services.ClientFunctions;
using MyAbilityFirst.Services.Common;
using NWebsec.Mvc.HttpHeaders.Csp;
using System.Web.Mvc;

namespace MyAbilityFirst.Controllers
{
	[Authorize(Roles = "Client")]
	[Csp(Enabled = false)]
	public class JobController : Controller
	{

		#region Fields

		private readonly IClientService _clientServices;
		private readonly IPresentationService _viewModelServices;
		private readonly IMapper _mapper;

		#endregion

		#region Ctor

		public JobController(IClientService clientServices, IPresentationService viewModelServices, IMapper mapper)
		{
			this._clientServices = clientServices;
			this._viewModelServices = viewModelServices;
			this._mapper = mapper;
		}

		#endregion

		#region Actions

		#endregion

		#region Helper

		private JobViewModel mapJobToJobViewModel(Job job)
		{
			JobViewModel model = new JobViewModel();
			model = _mapper.Map<Job, JobViewModel>(job);
			return model;
		}

		private JsonResult attachmentProcess(string url)
		{
			bool isUploaded = false;
			if (url != null)
			{
				isUploaded = true;
				string message = "100% complete";

				return Json(new
				{
					statusCode = 200,
					status = "File uploaded.",
					file = url,
					isUploaded = isUploaded,
					message = message
				}, "text/html");

			}
			else
			{
				string message = "Error";
				return Json(new
				{
					statusCode = 500,
					status = "Error uploading image.",
					file = string.Empty,
					isUploaded = isUploaded,
					message = message
				}, "text/html");
			}
		}
		#endregion

	}
}