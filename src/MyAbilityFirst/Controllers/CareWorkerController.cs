﻿using AutoMapper;
using System.Collections.Generic;
using System.Web.Mvc;
using Cenozoic.Models;
using Cenozoic.Services;
using MyAbilityFirst.Domain;
using MyAbilityFirst.Services.CareWorkerFunctions;
using MyAbilityFirst.Services.Common;

namespace MyAbilityFirst.Controllers
{
	[Authorize(Roles = "CareWorker")]
	public class CareWorkerController : Controller
	{

		#region Fields

		private readonly IMapper _mapper;
		private readonly ICareWorkerService _careWorkerServices;
		private readonly IPresentationService _categoryServices;
		private readonly IUserService _userServices;

		#endregion

		#region Ctor

		public CareWorkerController(ICareWorkerService careWorkerServices, IMapper mapper, IPresentationService categoryServices, IUserService userServices)
		{
			this._careWorkerServices = careWorkerServices;
			this._mapper = mapper;
			this._categoryServices = categoryServices;
			this._userServices = userServices;
		}

		#endregion

		#region Actions

		[HttpGet, Route("careworker/editprofile")]
		public ActionResult EditProfile()
		{
			CareWorkerDetailsViewModel vm = mapCareWorkerToVM();
			return View(vm);
		}

		[HttpPost, Route("careworker/editprofile")]
		[ValidateAntiForgeryToken]
		public ActionResult EditProfile(CareWorkerDetailsViewModel vm)
		{
			if (ModelState.IsValid)
			{
				CareWorker updatedCareWorker = mapVMToCareWorker(vm);
				var careWorkerID = this.GetLoggedInUser().ID;
				_careWorkerServices.ReplaceAllEmploymentHistories(careWorkerID, vm.EmploymentHistories);
				_careWorkerServices.ReplaceAllEmploymentFormalEducations(careWorkerID, vm.EmploymentFormalEducations);
				_careWorkerServices.ReplaceAllEmploymentReferences(careWorkerID, vm.EmploymentReferences);
				_careWorkerServices.ReplaceAllEmploymentAchievements(careWorkerID, vm.EmploymentAchievements);
				_careWorkerServices.ReplaceAllAvailabilities(careWorkerID, vm.Availabilities);
				_userServices.ReplaceAllUserSubCategories(careWorkerID, vm.PostedSubCategoryIDs, new List<UserSubcategory>());
				_careWorkerServices.UpdateCareWorker(updatedCareWorker);
			}
			return RedirectToAction("MyAccount");
		}

		[HttpGet, Route("careworker/myaccount")]
		public ActionResult MyAccount()
		{
			CareWorkerDetailsViewModel vm = mapCareWorkerToVM();
			return View(vm);
		}

		[HttpPost]
		public ActionResult NewEmploymentHistory(CareWorkerDetailsViewModel vm)
		{
			EmploymentHistory newEmploymentHistory = new EmploymentHistory();
			newEmploymentHistory.CareWorkerID = this.GetLoggedInUser().ID;
			vm.EmploymentHistories = vm.EmploymentHistories ?? new List<EmploymentHistory>();
			vm.EmploymentHistories.Add(newEmploymentHistory);
			return PartialView("_NewEmploymentHistory", vm);
		}

		[HttpPost]
		public ActionResult NewEmploymentFormalEducation(CareWorkerDetailsViewModel vm)
		{
			EmploymentFormalEducation newEmploymentFormalEducation = new EmploymentFormalEducation();
			newEmploymentFormalEducation.CareWorkerID = this.GetLoggedInUser().ID;
			vm.EmploymentFormalEducations = vm.EmploymentFormalEducations ?? new List<EmploymentFormalEducation>();
			vm.FormalEducationDropDownList = _categoryServices.GetSubCategorySelectList("FormalEducation");
			vm.EmploymentFormalEducations.Add(newEmploymentFormalEducation);
			return PartialView("_NewEmploymentFormalEducation", vm);
		}

		[HttpPost]
		public ActionResult NewEmploymentReference(CareWorkerDetailsViewModel vm)
		{
			EmploymentReference newEmploymentReference = new EmploymentReference();
			newEmploymentReference.CareWorkerID = this.GetLoggedInUser().ID;
			vm.EmploymentReferences = vm.EmploymentReferences ?? new List<EmploymentReference>();
			vm.EmploymentReferences.Add(newEmploymentReference);
			return PartialView("_NewEmploymentReference", vm);
		}

		[HttpPost]
		public ActionResult NewEmploymentAchievement(CareWorkerDetailsViewModel vm)
		{
			EmploymentAchievement newEmploymentAchievement = new EmploymentAchievement();
			newEmploymentAchievement.CareWorkerID = this.GetLoggedInUser().ID;
			vm.EmploymentAchievements = vm.EmploymentAchievements ?? new List<EmploymentAchievement>();
			vm.EmploymentAchievements.Add(newEmploymentAchievement);
			return PartialView("_NewEmploymentAchievement", vm);
		}

		#endregion

		#region Actions-Ratings

		[HttpGet]
		public ActionResult RatingDetails(int bookingID)
		{
			var carer = _careWorkerServices.RetrieveCareWorker(this.GetLoggedInUser().ID);
			var booking = carer.GetBooking(bookingID);
			if (booking == null)
				return View("Error");

			var rating = booking.Rating;
			var vm = new RatingViewModel();
			vm = _mapper.Map<Rating, RatingViewModel>(rating);

			return View(vm);
		}
		#endregion

		#region Helpers

		private CareWorkerDetailsViewModel mapCareWorkerToVM()
		{
			CareWorkerDetailsViewModel vm = new CareWorkerDetailsViewModel();
			_mapper.Map<CareWorker, CareWorkerDetailsViewModel>(_careWorkerServices.RetrieveCareWorker(this.GetLoggedInUser().ID), vm);
			vm.EmploymentHistories = vm.EmploymentHistories ?? new List<EmploymentHistory>();
			vm.EmploymentFormalEducations = vm.EmploymentFormalEducations ?? new List<EmploymentFormalEducation>();
			vm.EmploymentReferences = vm.EmploymentReferences ?? new List<EmploymentReference>();
			vm.EmploymentAchievements = vm.EmploymentAchievements ?? new List<EmploymentAchievement>();
			vm.Availabilities = vm.Availabilities ?? new List<Availability>();
			return vm;
		}

		private CareWorker mapVMToCareWorker(CareWorkerDetailsViewModel vm)
		{
			return _mapper.Map(vm, _careWorkerServices.RetrieveCareWorker(this.GetLoggedInUser().ID));
		}

		private JsonResult attachmentProcess(string url)
		{
			bool isUploaded = false;
			if (url != null)
			{
				isUploaded = true;
				string message = "100% complete";

				return Json(new
				{
					statusCode = 200,
					status = "File uploaded.",
					file = url,
					isUploaded = isUploaded,
					message = message
				}, "text/html");

			}
			else
			{
				string message = "Error";
				return Json(new
				{
					statusCode = 500,
					status = "Error uploading image.",
					file = string.Empty,
					isUploaded = isUploaded,
					message = message
				}, "text/html");
			}
		}

		#endregion

	}
}
