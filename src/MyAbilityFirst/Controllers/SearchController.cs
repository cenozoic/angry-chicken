﻿using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cenozoic.Models;
using MyAbilityFirst.Services.SearchFunctions;
using MyAbilityFirst.Domain;



namespace MyAbilityFirst.Controllers
{
	[Authorize]
	public class SearchController : Controller
	{

		#region Fields

		private readonly IMapper _mapper;
		private readonly IMAFSearchService _searchServices;

		#endregion

		#region Ctor

		public SearchController(IMapper mapper, IMAFSearchService searchServices)
		{
			this._mapper = mapper;
			this._searchServices = searchServices;
		}

		#endregion

		#region CareWorker Search

		public async Task<JsonResult> SearchCareWorker(SearchViewModel vm)
		{
			vm.UserID = this.GetLoggedInUser().ID;
			vm = _mapper.Map(vm, vm);
			vm = await updateLatLngWithPlaceID(vm);
			var res = getJsonResult("", vm.UserID > 0, "SearchCareWorker", vm);
			setResultsAsAllowGet(ref res);
			return res;
		}

		[HttpPost]
		public JsonResult SearchCareWorkerPaged(SearchViewModel vm)
		{
			vm.UserID = this.GetLoggedInUser().ID;
			vm.SearchResults = _searchServices.GetCareWorkerSearchResults(
				vm.UserID,
				vm.SearchTerm,
				vm.MinLong,
				vm.MinLat,
				vm.MaxLong,
				vm.MaxLat,
				null,
				null,
				null,
				vm.PostedSubcategoryIDs,
				vm.SortOption,
				vm.PageNumber,
				vm.PageSize
			);
			var res = getJsonResultPaged("", vm.UserID > 0, "_SearchResults", vm);
			setResultsAsAllowGet(ref res);
			return res;
		}

		#endregion

		#region Predictive Search

		[HttpGet]
		public JsonResult SearchSubcategoryJson(string subcategoryName)
		{
			List<Subcategory> list = _searchServices.GetSearchableSubCategories(subcategoryName);
			var res = Json(_mapper.Map<List<AutoCompleteViewModel>>(list));
			return setResultsAsAllowGet(ref res);
		}

		[HttpGet]
		public JsonResult SearchCareWorkerNameJson(string careWorkerName)
		{
			var loggedInUser = this.GetLoggedInUser();
			JsonResult res = Json(
				_searchServices.GetCareWorkerSearchResults(
				loggedInUser.ID,
				careWorkerName, null, null, null, null, loggedInUser.Address.Longitude, loggedInUser.Address.Latitude, null, null,
				SortOption.MostRecent)
			);
			return setResultsAsAllowGet(ref res);
		}

		[HttpGet]
		public async Task<JsonResult> SearchLocationJson(string locationName)
		{
			var resobj = await _searchServices.GetSearchableLocations(locationName);
			JsonResult res = Json(resobj);
			return setResultsAsAllowGet(ref res);
		}

		#endregion

		#region Helpers

		private JsonResult setResultsAsAllowGet(ref JsonResult result)
		{
			result.ContentEncoding = System.Text.Encoding.UTF8;
			result.ContentType = "application/json";
			result.MaxJsonLength = 100000;
			result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
			result.RecursionLimit = 50;
			return result;
		}

		private JsonResult getJsonResult(string html, bool success, string partialViewName, SearchViewModel vm)
		{
			if (success && html.Length == 0 && partialViewName.Length > 0 && vm != null)
				html = this.RenderViewToString(partialViewName, vm);

			return
				Json(new
				{
					Html = html,
					Success = success,
					Latitude = vm.Latitude,
					Longitude = vm.Longitude,
					HomeLatitude = vm.HomeLatitude,
					HomeLongitude = vm.HomeLongitude
				});
		}

		private JsonResult getJsonResultPaged(string html, bool success, string partialViewName, SearchViewModel vm)
		{
			if (success && html.Length == 0 && partialViewName.Length > 0 && vm != null)
				html = this.RenderViewToString(partialViewName, vm);

			return
				Json(new
				{
					Html = html,
					Success = success,
					PageNumber = vm.PageNumber,
					PageCount = vm.SearchResults == null ? 0 : vm.SearchResults.PageCount,
					ResultsCount = vm.SearchResults == null ? 0 : vm.SearchResults.TotalItemCount
				});
		}

		private async Task<SearchViewModel> updateLatLngWithPlaceID(SearchViewModel vm) {
			if (vm.PlaceID != null)
			{
				PlaceDetails placeDetails = await _searchServices.GetLatLngFromPlaceID(vm.PlaceID);
				vm.Latitude = (decimal?)placeDetails.lat;
				vm.Longitude = (decimal?)placeDetails.lng;
			}
			return vm;
		}

		#endregion

	}

}