﻿using AutoMapper;
using System.Linq;
using Cenozoic.Models;
using MyAbilityFirst.Domain;

namespace MyAbilityFirst.Services.Common
{
	public class SearchMappingProfile : Profile
	{

		#region Fields

		private readonly IPresentationService _presentationService;

		public override string ProfileName => "SearchMappingProfile";

		#endregion

		#region Ctor

		public SearchMappingProfile(IPresentationService presentationService)
		{
			this._presentationService = presentationService;
			this.MapSearchCriteria();
			this.MapAutoCompleteCriteria();
		}

		#endregion

		#region Helpers

		private void MapSearchCriteria() 
		{
			CreateMap<SearchViewModel, SearchViewModel>()
				.ForMember(dest => dest.GenderList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("Gender")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousGenderList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.GenderList);
						dest.GenderLabel = _presentationService.GetCategoryDisplayName(dest.GenderList.First().CategoryID);
					})
				.ForMember(dest => dest.LanguageList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("Language")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousLanguageList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.LanguageList);
						dest.LanguageLabel = _presentationService.GetCategoryDisplayName(dest.LanguageList.First().CategoryID);
					})
				.ForMember(dest => dest.CultureList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("Culture")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousCultureList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.CultureList);
						dest.CultureLabel = _presentationService.GetCategoryDisplayName(dest.CultureList.First().CategoryID);
					})
				.ForMember(dest => dest.ReligionList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("Religion")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousReligionList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.ReligionList);
						dest.ReligionLabel = _presentationService.GetCategoryDisplayName(dest.ReligionList.First().CategoryID);
					})
				.ForMember(dest => dest.PersonalityList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("Personality")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousPersonalityList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.PersonalityList);
						dest.PersonalityLabel = _presentationService.GetCategoryDisplayName(dest.PersonalityList.First().CategoryID);
					})
				.ForMember(dest => dest.NursingQualificationsList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("NursingQualifications")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousNursingQualificationsList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.NursingQualificationsList);
						dest.NursingQualificationsLabel = _presentationService.GetCategoryDisplayName(dest.NursingQualificationsList.First().CategoryID);
					})
				.ForMember(dest => dest.PersonalCareQualificationsList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("PersonalCareQualifications")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousPersonalCareQualificationsList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.PersonalCareQualificationsList);
						dest.PersonalCareQualificationsLabel = _presentationService.GetCategoryDisplayName(dest.PersonalCareQualificationsList.First().CategoryID);
					})
				.ForMember(dest => dest.OtherQualificationsList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("OtherQualifications")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousOtherQualificationsList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.OtherQualificationsList);
						dest.OtherQualificationsLabel = _presentationService.GetCategoryDisplayName(dest.OtherQualificationsList.First().CategoryID);
					})
				.ForMember(dest => dest.VerificationChecksList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("VerificationChecks")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousVerificationChecksList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.VerificationChecksList);
						dest.VerificationChecksLabel = _presentationService.GetCategoryDisplayName(dest.VerificationChecksList.First().CategoryID);
					})
				.ForMember(dest => dest.OtherPreferenceList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("OtherPreference")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousOtherPreferenceList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.OtherPreferenceList);
						dest.OtherPreferenceLabel = _presentationService.GetCategoryDisplayName(dest.OtherPreferenceList.First().CategoryID);
					})
				.ForMember(dest => dest.DisabilityCareExperienceList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("DisabilityCareExperience")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousDisabilityCareExperienceList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.DisabilityCareExperienceList);
						dest.DisabilityCareExperienceLabel = _presentationService.GetCategoryDisplayName(dest.DisabilityCareExperienceList.First().CategoryID);
					})
				.ForMember(dest => dest.MentalHealthCareExperienceList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("MentalHealthCareExperience")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousMentalHealthCareExperienceList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.MentalHealthCareExperienceList);
						dest.MentalHealthCareExperienceLabel = _presentationService.GetCategoryDisplayName(dest.MentalHealthCareExperienceList.First().CategoryID);
					})
				.ForMember(dest => dest.AgedCareExperienceList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("AgedCareExperience")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousAgedCareExperienceList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.AgedCareExperienceList);
						dest.AgedCareExperienceLabel = _presentationService.GetCategoryDisplayName(dest.AgedCareExperienceList.First().CategoryID);
					})
				.ForMember(dest => dest.ChronicMedicalConditionsCareExperienceList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("ChronicMedicalConditionsCareExperience")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousChronicMedicalConditionsCareExperienceList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.ChronicMedicalConditionsCareExperienceList);
						dest.ChronicMedicalConditionsCareExperienceLabel = _presentationService.GetCategoryDisplayName(dest.ChronicMedicalConditionsCareExperienceList.First().CategoryID);
					})
				.ForMember(dest => dest.InterestList, opt => opt.MapFrom(src => _presentationService.GetSubCategoryList("Interest")))
					.AfterMap((src, dest) =>
					{
						dest.PreviousInterestList = _presentationService.GetCrossReferenceSubcategoryList(src.PostedSubcategoryIDs, dest.InterestList);
						dest.InterestLabel = _presentationService.GetCategoryDisplayName(dest.InterestList.First().CategoryID);
					})
				.ForMember(dest => dest.UserID, opt => opt.MapFrom(src => src.UserID))
					.AfterMap((src, dest) =>
					{
						var Address = _presentationService.GetUserAddress(dest.UserID);
						dest.HomeLongitude = Address == null ? null : (decimal?) Address.Longitude;
						dest.HomeLatitude = Address == null ? null : (decimal?) Address.Latitude;
						if (Address != null && Address.FullAddress != null) {
							dest.Latitude = src.Latitude == null ? Address.Latitude : src.Latitude;
							dest.Longitude = src.Longitude == null ? Address.Longitude : src.Longitude;
						} else {
							// set default as Brisbane
							dest.Latitude = src.Latitude == null ? (decimal?) -27.469770700 : src.Latitude;
							dest.Longitude = src.Longitude == null ? (decimal?) 153.025123500 : src.Longitude;
						}
					})
				.ForMember(dest => dest.PageNumber, opt => opt.Condition(src => src.PageNumber > 0))
				.ForMember(dest => dest.PageSize, opt => opt.Condition(src => src.PageSize > 0))
				.ForAllOtherMembers(opt => opt.Ignore())
				;
		}

		private void MapAutoCompleteCriteria()
		{
			CreateMap<Subcategory, AutoCompleteViewModel>()
				.ForMember(dest => dest.SubcategoryName, opt => opt.MapFrom(src => src.Name))
				.ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => _presentationService.GetCategoryDisplayName(src.CategoryID)))
				.ForMember(dest => dest.SubcategoryID, opt => opt.MapFrom(src => src.ID));
		}

		#endregion

	}
}

