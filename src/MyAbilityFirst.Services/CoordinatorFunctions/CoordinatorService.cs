﻿using AutoMapper;
using System;
using Cenozoic.Models;
using Cenozoic.Infrastructure;
using Cenozoic.Infrastructure.Communication;
using MyAbilityFirst.Infrastructure.Data;
using MyAbilityFirst.Domain;
using MyAbilityFirst.Services.ClientFunctions;



namespace MyAbilityFirst.Services.CoordinatorFunctions
{
	public class CoordinatorService : ICoordinatorService
	{

		#region Fields

		private readonly IWriteEntities _entities;
		private IMapper _mapper;
		private readonly BookingData _bookingData;
		private readonly IClientService _clientServices;
		private readonly IEmailService _emailServices;

		#endregion


		#region Ctor

		public CoordinatorService(IWriteEntities entities, IMapper mapper, BookingData bookingData, IClientService clientService)
		{
			this._entities = entities;
			this._mapper = mapper;
			this._bookingData = bookingData;
			this._clientServices = clientService;
		}

		#endregion

		#region profile

		public CoordinatorDetailsViewModel GetCoordinatorVM(Coordinator currentCoordinator)
		{
			CoordinatorDetailsViewModel model = new CoordinatorDetailsViewModel();
			model = _mapper.Map<Coordinator, CoordinatorDetailsViewModel>(currentCoordinator);
			return model;
		}


		public void UpdateProfile(Coordinator updatedCoordinator)
		{
			updatedCoordinator.Status = UserStatus.Active;
			updatedCoordinator.UpdatedAt = DateTime.Now;
			this._entities.Update(updatedCoordinator);
			this._entities.Save();
		}

		#endregion

		#region review-approval

		public void ApproveRating(int coordinatorID, int ratingID)
		{
			Rating currentRating = this._entities.Single<Rating>(a => a.ID == ratingID);
			currentRating.Status = RatingStatus.Approved;
			currentRating.ApprovedDate = DateTime.Now;
			currentRating.CoordinatorID = coordinatorID;
			var booking = this._entities.Single<Booking>(b => b.ID == currentRating.BookingID);
			var careWorker = this._entities.Single<CareWorker>(c => c.ID == booking.WorkerID);
			careWorker.AddOverallRating(currentRating.OverallScore);

			this._entities.Update(careWorker);
			this._entities.Save();
		}
 
		public void ApproveCareWorker(int coordinatorID, int careWorkerID)
		{
			var careWorker = this._entities.Single<CareWorker>(c => c.ID == careWorkerID);
			if (careWorker == null)
				throw new ArgumentNullException("careWorker");
			careWorker.ApprovedDate = DateTime.Now;
			careWorker.CoordinatorID = coordinatorID;
			careWorker.Status = UserStatus.Active;

			this._entities.Update(careWorker);
			this._entities.Save();
		}

		#endregion

	}
}
