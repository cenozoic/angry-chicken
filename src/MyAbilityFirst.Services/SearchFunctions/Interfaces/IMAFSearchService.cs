﻿using PagedList;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cenozoic.Models;
using MyAbilityFirst.Domain;


namespace MyAbilityFirst.Services.SearchFunctions
{
	public interface IMAFSearchService
	{
		PagedList<CareWorkerSearchResult> GetCareWorkerSearchResults(
			int ownerUserID,
			string SearchTerm = "",
			decimal? MinLong = null,
			decimal? MinLat = null,
			decimal? MaxLong = null,
			decimal? MaxLat = null,
			decimal? Longitude = null,
			decimal? Latitude = null,
			float? radiusInKm = 0,
			int[] PostedSubcategoryIDs = null,
			SortOption sortOption = SortOption.Closest,
			int pageNumber = 1,
			int pageSize = PageSize.DefaultSearchPageSize
		);
		List<Subcategory> GetSearchableSubCategories(string searchString);
		Task<List<PlaceAutoComplete>> GetSearchableLocations(string searchString);
		Task<PlaceDetails> GetLatLngFromPlaceID(string placeID);
	}
}
