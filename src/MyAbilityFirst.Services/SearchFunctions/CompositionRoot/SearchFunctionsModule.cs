﻿using Autofac;

namespace MyAbilityFirst.Services.SearchFunctions
{
	public class MAFSearchFunctionsModule : Autofac.Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			// register SearchFunctionServices
			builder
					.RegisterType<MAFSearchService>()
					.As<IMAFSearchService>();
		}
	}
}
