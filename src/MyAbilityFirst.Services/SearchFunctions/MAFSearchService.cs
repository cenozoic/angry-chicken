﻿using PagedList;
using System.Data.Entity.Spatial;
using System.Data.SqlClient;
using System.Linq;
using Cenozoic.Models;
using Cenozoic.Services;
using Cenozoic.Infrastructure;
using MyAbilityFirst.Domain;


namespace MyAbilityFirst.Services.SearchFunctions
{
	public class MAFSearchService : SearchService, IMAFSearchService
	{

		#region Fields

		private readonly IUserService _userServices;

		#endregion

		#region Ctor

		public MAFSearchService(IWriteEntities entities, ICacheService cacheServices, IUserService userServices): base(entities, cacheServices)
		{
			this._userServices = userServices;
		}

		#endregion

		#region Build and Execute

		public PagedList<CareWorkerSearchResult> GetCareWorkerSearchResults(
			int ownerUserID,
			string SearchTerm = "",
			decimal? MinLong = null,
			decimal? MinLat = null,
			decimal? MaxLong = null,
			decimal? MaxLat = null,
			decimal? Longitude = null,
			decimal? Latitude = null,
			float? radiusInKm = 0,
			int[] PostedSubcategoryIDs = null,
			SortOption sortOption = SortOption.Closest,
			int pageNumber = 1, 
			int pageSize = PageSize.DefaultSearchPageSize
		)
		{
			IQueryable<CareWorker> query = GetAllUsers<CareWorker>();

			// Defaults to user home location for distance calculation
			if (Latitude == null || Longitude == null)
			{
				var user = _userServices.GetUser(ownerUserID);
				Address homeAddress = user.Address;
				Latitude = homeAddress.Latitude;
				Longitude = homeAddress.Longitude;
			}

			if (!string.IsNullOrWhiteSpace(SearchTerm))
				query = FilterUserByName(query, SearchTerm);
			if (query == null) return null;

			if (MinLong != null && MinLat != null && MaxLong != null && MaxLat != null)
				query = FilterUserByLatLngBounds(query, MinLong, MinLat, MaxLong, MaxLat);
			if (query == null) return null;

			if (Longitude != null && Latitude != null && radiusInKm != null && radiusInKm >= 0)
				query = FilterUserByDistance(query, Longitude, Latitude, radiusInKm);
			if (query == null) return null;

			if (PostedSubcategoryIDs != null && PostedSubcategoryIDs.Length > 0) 
			{
				var PostedGenderIDs = GetPostedIDsAndRemoveFromArray("Gender", ref PostedSubcategoryIDs);
				if (PostedGenderIDs != null && PostedGenderIDs.Count() > 0)
					query = FilterCareWorkerByGender(query, PostedGenderIDs);
				if (query == null) return null;

				var PostedLanguageIDs = GetPostedIDsAndRemoveFromArray("Language", ref PostedSubcategoryIDs);
				if (PostedLanguageIDs != null && PostedLanguageIDs.Count() > 0)
					query = FilterCareWorkerByLanguage(query, PostedLanguageIDs);
				if (query == null) return null;

				var PostedCultureIDs = GetPostedIDsAndRemoveFromArray("Culture", ref PostedSubcategoryIDs);
				if (PostedCultureIDs != null && PostedCultureIDs.Count() > 0)
					query = FilterCareWorkerByCulture(query, PostedCultureIDs);
				if (query == null) return null;

				var PostedReligionIDs = GetPostedIDsAndRemoveFromArray("Religion", ref PostedSubcategoryIDs);
				if (PostedReligionIDs != null && PostedReligionIDs.Count() > 0)
					query = FilterCareWorkerByReligion(query, PostedReligionIDs);
				if (query == null) return null;

				var PostedPersonalityIDs = GetPostedIDsAndRemoveFromArray("Personality", ref PostedSubcategoryIDs);
				if (PostedPersonalityIDs != null && PostedPersonalityIDs.Count() > 0)
					query = FilterCareWorkerByPersonality(query, PostedPersonalityIDs);
				if (query == null) return null;

				if (PostedSubcategoryIDs != null && PostedSubcategoryIDs.Count() > 0)
					query = FilterUserByUserSubcategory<CareWorker>(query, PostedSubcategoryIDs);
				if (query == null) return null;
			}

			IQueryable<CareWorkerSearchResult> res = ProjectQueryAsCareWorkerSearchResults(ownerUserID, query, Longitude, Latitude);

			bool firstSort = true;
			switch (sortOption)
			{
				case SortOption.Rating:
					res = OrderCareWorkerByRating(res, SortOrder.Descending, ref firstSort);
					break;
				case SortOption.Closest:
					res = OrderCareWorkerByDistanceFromLatLong(res, SortOrder.Ascending, Longitude, Latitude, ref firstSort);
					break;
				default:
					res = OrderCareWorkerByDateModified(res, SortOrder.Descending, ref firstSort);
					break;
			}
			return GetResultsAsPagedList(res, pageNumber, pageSize);
		}
		#endregion

	
		#region CareWorker filters

		private IQueryable<CareWorker> FilterCareWorkerByGender(IQueryable<CareWorker> query, int[] genderIDs)
		{
			if (genderIDs == null || genderIDs.Count() != 1)
				return null;
			int genderID = genderIDs[0];
			return query.Where(cw => cw.GenderID == genderID);
		}

		private IQueryable<CareWorker> FilterCareWorkerByLanguage(IQueryable<CareWorker> query, int[] languageIDs)
		{
			if (languageIDs == null || languageIDs.Count() < 1 || languageIDs.Count() > 2)
				return null;

			int languageID0 = languageIDs[0];
			
			if (languageIDs.Count() == 2) 
			{
				int languageID1 = languageIDs[1];
				return query.Where(cw =>
					cw.FirstLanguageID == languageID0 && cw.SecondLanguageID == languageID1 ||
					cw.FirstLanguageID == languageID1 && cw.SecondLanguageID == languageID0);
			}
			else 
			{
				return query.Where(cw =>
					cw.FirstLanguageID == languageID0 || cw.SecondLanguageID == languageID0);
			}
		}

		private IQueryable<CareWorker> FilterCareWorkerByCulture(IQueryable<CareWorker> query, int[] cultureIDs)
		{
			if (cultureIDs == null || cultureIDs.Count() != 1)
				return null;
			int cultureID = cultureIDs[0];
			return query.Where(cw => cw.CultureID == cultureID);
		}

		private IQueryable<CareWorker> FilterCareWorkerByReligion(IQueryable<CareWorker> query, int[] religionIDs)
		{
			if (religionIDs == null || religionIDs.Count() != 1)
				return null;
			int religionID = religionIDs[0];
			return query.Where(cw => cw.ReligionID == religionID);
		}

		private IQueryable<CareWorker> FilterCareWorkerByPersonality(IQueryable<CareWorker> query, int[] personalityIDs)
		{
			if (personalityIDs == null || personalityIDs.Count() != 1)
				return null;
			int personalityID = personalityIDs[0];
			return query.Where(cw => cw.PersonalityID == personalityID);
		}

		private IQueryable<CareWorkerSearchResult> OrderCareWorkerByRating(IQueryable<CareWorkerSearchResult> query, SortOrder sortOrder, ref bool firstSort)
		{
			query = query.OrderBy(
				cw => cw.AverageRating,
				sortOrder,
				ref firstSort
			);
			return query;
		}

		private IQueryable<CareWorkerSearchResult> OrderCareWorkerByDistanceFromLatLong(IQueryable<CareWorkerSearchResult> query, SortOrder sortOrder, decimal? longitude, decimal? latitude, ref bool firstSort)
		{
			if (longitude == null || latitude == null)
				return query;

			query = query.OrderBy(
				cw => DbGeography.FromText("POINT(" + longitude.ToString() + " " + latitude.ToString() + ")", 4326).Distance(cw.Address.GeoPoint) / 1000,
				sortOrder,
				ref firstSort
			);
			return query;
		}

		private IQueryable<CareWorkerSearchResult> OrderCareWorkerByDateModified(IQueryable<CareWorkerSearchResult> query, SortOrder sortOrder, ref bool firstSort)
		{
			query = query.OrderBy(
				cw => cw.UpdatedAt,
				sortOrder,
				ref firstSort
			);
			return query;
		}

		private IQueryable<CareWorkerSearchResult> ProjectQueryAsCareWorkerSearchResults(int ownerUserID, IQueryable<CareWorker> query, decimal? Longitude = null, decimal? Latitude = null)
		{

			IQueryable<Shortlist> shortlists = _entities.GetLazy<Shortlist>();
			shortlists.Where(sl => sl.OwnerUserID == ownerUserID);

			return
				from q in query
				from sl in shortlists
					.Where(sl2 => sl2.OwnerUserID == ownerUserID && q.ID == sl2.SelectedUserID && sl2.Selected).DefaultIfEmpty()
				select new CareWorkerSearchResult
				{
					CareWorkerID = q.ID,
					Address = q.Address,
					UpdatedAt = q.UpdatedAt,
					FirstName = q.FirstName,
					LastName = q.LastName,
					Description = q.Description,
					DistanceFromSearchInKm = (Latitude == null || Longitude == null) ? null : DbGeography.FromText("POINT(" + Longitude.ToString() + " " + Latitude.ToString() + ")", 4326).Distance(q.Address.GeoPoint) / 1000,
					WithinServiceCoverage = (Latitude == null || Longitude == null) ? true : q.LocationCoverageRadius >= DbGeography.FromText("POINT(" + Longitude.ToString() + " " + Latitude.ToString() + ")", 4326).Distance(q.Address.GeoPoint) / 1000,
					AverageRating = q.RatingCount == 0 ? 0 : q.TotalRating / q.RatingCount,
					Longitude = q.Address.Longitude,
					Latitude = q.Address.Latitude,
					Shortlisted = (bool?) sl.Selected,
					PictureURL = "https://www.w3schools.com/bootstrap/img_avatar3.png"
				};
		}

		#endregion

	}
}
