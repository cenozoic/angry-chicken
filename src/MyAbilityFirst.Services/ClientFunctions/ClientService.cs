﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Cenozoic.Models;
using Cenozoic.Services;
using Cenozoic.Infrastructure;
using MyAbilityFirst.Domain;
using MyAbilityFirst.Services.Common;


namespace MyAbilityFirst.Services.ClientFunctions
{
	public class ClientService : IClientService
	{

		#region Fields

		private readonly IWriteEntities _entities;
		private readonly IMapper _mapper;
		private readonly IUserService _userServices;

		#endregion

		#region Ctor

		public ClientService(IWriteEntities entities, IMapper mapper, IUserService userServices)
		{
			this._entities = entities;
			this._mapper = mapper;
			this._userServices = userServices;
		}

		#endregion

		#region Client Services

		public Client RetrieveClient(int clientID)
		{
			return this._entities.Single<Client>(c => c.ID == clientID);
		}

		public Client RetrieveClientByLoginID(string identityId)
		{
			return this._entities.Single<Client>(c => c.LoginIdentityId == identityId);
		}

		public Client UpdateClient(Client clientData)
		{
			clientData.Status = UserStatus.Active;
			clientData.UpdatedAt = DateTime.Now;
			this._entities.Update(clientData);
			this._entities.Save();
			return clientData;
		}

		public Patient CreatePatient(int clientID, Patient patientData)
		{
			Client parentClient = RetrieveClient(clientID);
			if (parentClient != null)
			{
				Patient newPatient = parentClient.AddNewPatient(patientData);
				this._entities.Update(parentClient);
				this._entities.Save();
				return newPatient;
			}
			return null;
		}

		public Patient RetrievePatient(int clientID, int patientID)
		{
			Client parentClient = RetrieveClient(clientID);
			if (parentClient != null)
			{
				return parentClient.GetExistingPatient(patientID);
			}
			return null;
		}

		public List<Patient> RetrieveAllPatients(int clientID)
		{
			Client parentClient = RetrieveClient(clientID);
			if (parentClient != null)
			{
				return parentClient.GetAllPatients();
			}
			return null;
		}

		public Patient UpdatePatient(int clientID, Patient patientData)
		{
			Client parentClient = RetrieveClient(clientID);
			if (parentClient != null)
			{
				parentClient.UpdateExistingPatient(patientData);
				this._entities.Update(parentClient);
				this._entities.Save();
				return patientData;
			}
			return null;

		}

		public Contact CreateContact(int clientID, int patientID, Contact contactData)
		{
			Client parentClient = RetrieveClient(clientID);
			if (parentClient != null)
			{
				Patient parentPatient = parentClient.GetExistingPatient(patientID);
				if (parentPatient != null)
				{
					Contact newContact = parentPatient.AddNewContact(contactData);
					parentClient.UpdateExistingPatient(parentPatient);
					this._entities.Update(parentClient);
					this._entities.Save();
					return newContact;
				}
				return null;
			}
			return null;
		}

		public Contact RetrieveContact(int clientID, int patientID, int contactID)
		{
			Client parentClient = RetrieveClient(clientID);
			if (parentClient != null)
			{
				Patient parentPatient = parentClient.GetExistingPatient(patientID);
				if (parentPatient != null)
				{
					Contact contact = parentPatient.GetExistingContact(contactID);
					return contact;
				}
				return null;
			}
			return null;
		}

		public List<Contact> RetrieveAllContacts(int clientID, int patientID)
		{
			Client parentClient = RetrieveClient(clientID);
			if (parentClient != null)
			{
				Patient parentPatient = parentClient.GetExistingPatient(patientID);
				if (parentPatient != null)
				{
					var contacts = parentPatient.GetAllExistingContacts();
					return contacts;
				}
				return null;
			}
			return null;
		}

		public Contact UpdateContact(int clientID, int patientID, Contact contactData)
		{
			Client parentClient = RetrieveClient(clientID);
			if (parentClient != null)
			{
				Patient parentPatient = parentClient.GetExistingPatient(patientID);
				if (parentPatient != null)
				{
					Contact existingContact = RetrieveContact(clientID, patientID, contactData.ID);
					_mapper.Map(contactData, existingContact);
					parentPatient.UpdateExistingContact(existingContact);
					parentClient.UpdateExistingPatient(parentPatient);
					this._entities.Update(existingContact);
					this._entities.Save();
					return existingContact;
				}
				return null;
			}
			return null;
		}

		public bool DeleteContact(int clientID, int patientID, int contactID)
		{
			Client parentClient = RetrieveClient(clientID);
			if (parentClient != null)
			{
				Patient parentPatient = parentClient.GetExistingPatient(patientID);
				if (parentPatient != null)
				{
					Contact contact = parentPatient.RemoveContact(contactID);
					this._entities.Delete(contact);
					this._entities.Save();
					return true;
				}
				return false;
			}
			return false;
		}

		public List<Contact> ReplaceAllContacts(int clientID, int patientID, List<Contact> contacts)
		{
			List<Contact> existingContacts = RetrieveAllContacts(clientID, patientID) ?? new List<Contact>();
			contacts = contacts ?? new List<Contact>();
			IEnumerable<Contact> actionablecontacts = contacts.Except(existingContacts, new ContactComparer());
			// CREATE new Contact if old dataset doesn't have new contacts
			foreach (Contact contact in actionablecontacts)
			{
				contact.PatientID = patientID;
				CreateContact(clientID, contact.PatientID, contact);
			}
			// UPDATE Contact if already exists
			actionablecontacts = contacts.Intersect(existingContacts, new ContactComparer());
			foreach (Contact contact in actionablecontacts)
			{
				UpdateContact(clientID, contact.PatientID, contact);
			}
			// DELETE old Contact if new dataset doesn't have old contacts
			actionablecontacts = existingContacts.Except(contacts, new ContactComparer());
			foreach (Contact contact in actionablecontacts)
			{
				DeleteContact(clientID, contact.PatientID, contact.ID);
			}
			return contacts;
		}

		#endregion

		#region Job Services

		public Job DraftJob(int clientID, Job jobData)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;
			client.AddJob(jobData);
			SaveClient(client);
			return jobData;
		}

		public Job AdvertiseJob(int clientID, int jobID)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;

			var job = client.GetJob(jobID);
			if (job == null) return null;

			job.AdvertiseByClient();
			client.UpdateJob(job);
			SaveClient(client);
			return job;
		}

		public Job AssignPrimaryCarer(int clientID, int jobID, int careWorkerID)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;

			var job = client.GetJob(jobID);
			if (job == null) return null;

			job.AssignPrimaryCarerByClient(careWorkerID);

			// Send careworker notification of accepted application
			if (job.PrimaryWorkerID != 0)
				SendJobAlert(job.PrimaryWorkerID, client.DisplayName, job.ID, " Your job application (" + job.Title + ") have been accepted");
			
			client.UpdateJob(job);
			SaveClient(client);
			return job;
		}

		public Job ReassignPrimaryCarer(int clientID, int jobID, int careWorkerID)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;

			var job = client.GetJob(jobID);
			if (job == null) return null;

			var oldCarerID = job.PrimaryWorkerID;
			job.AssignPrimaryCarerByClient(careWorkerID);

			// Send notification to new and old careworker about change, new carer needs to confirm
			if (oldCarerID != 0)
				SendJobAlert(oldCarerID, client.DisplayName, job.ID, "You have been relieved as the primary carer for this job: (" + job.Title + ")");
			if (job.PrimaryWorkerID != 0)
				SendJobAlert(job.PrimaryWorkerID, client.DisplayName, job.ID, "You have been invited as the primary carer for this job: (" + job.Title + ")");
			
			client.UpdateJob(job);
			SaveClient(client);
			return job;
		}

		public JobNotification AcknowledgeJobChanges(int clientID, int jobID)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;

			var job = client.GetJob(jobID);
			if (job == null) return null;

			var clientJobAlert = GetJobAlert(clientID, jobID);
			if (clientJobAlert == null) return null;

			// Send careworker notification changes have been accepted
			if (job.PrimaryWorkerID != 0)
				SendJobAlert(job.PrimaryWorkerID, client.DisplayName, job.ID, " have agreed to the new proposed job changes");

			clientJobAlert.AcceptedChange = true;
			return _userServices.UpdateNotification<JobNotification>(clientJobAlert);

		}

		public Job DismissPrimaryCarer(int clientID, int jobID)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;

			var job = client.GetJob(jobID);
			if (job == null) return null;

			var oldCarerID = job.PrimaryWorkerID;
			job.DismissPrimaryCarerByClient();

			// Send old careworker termination notice
			if (oldCarerID != 0)
				SendJobAlert(oldCarerID, client.DisplayName, job.ID, "You have been relieved as the primary carer for this job: (" + job.Title + ")");

			client.UpdateJob(job);
			SaveClient(client);
			return job;
		}

		public Job AddNewJobSchedule(int clientID, int jobID, Schedule newSchedule)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;

			var job = client.GetJob(jobID);
			if (job == null) return null;

			job.NewScheduleByClient(newSchedule);

			// Send careworker notification of new schedule
			if (job.PrimaryWorkerID != 0)
				SendJobAlert(job.PrimaryWorkerID, client.DisplayName, job.ID, "A new schedule has been created for this job: (" + job.Title + ") " + newSchedule.Duration.Start.ToString() + " - " + newSchedule.Duration.Start.ToString());

			client.UpdateJob(job);
			SaveClient(client);
			return job;
		}

		public Job RescheduleJob(int clientID, int jobID, Schedule modifiedSchedule)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;

			var job = client.GetJob(jobID);
			if (job == null) return null;

			job.RescheduleByClient(modifiedSchedule);

			// Send careworker notification of reschedule request
			if (job.PrimaryWorkerID != 0)
				SendJobAlert(job.PrimaryWorkerID, client.DisplayName, job.ID, "An existing schedule has been modified for this job: (" + job.Title + ") " + modifiedSchedule.Duration.Start.ToString() + " - " + modifiedSchedule.Duration.Start.ToString());

			client.UpdateJob(job);
			SaveClient(client);
			return job;
		}
		
		public Job CancelJob(int clientID, int jobID)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;

			var job = client.GetJob(jobID);
			if (job == null) return null;

			job.CancelByClient();
			// Send careworker notification of cancelled job, if assigned to carer

			if (job.PrimaryWorkerID != 0)
				SendJobAlert(job.PrimaryWorkerID, client.DisplayName, job.ID, "This job have been cancelled: (" + job.Title + ")");

			client.UpdateJob(job);
			SaveClient(client);
			return job;
		}
		
		private void SaveClient(Client client) 
		{
			this._entities.Update(client);
			this._entities.Save();
		}

		private JobNotification GetJobAlert(int clientID, int jobID)
		{
			return _userServices.RetrieveNotification<JobNotification>(n => n.OwnerUserID == clientID && n.JobID == jobID);
		}

		private void SendJobAlert(int recipientID, string senderUserName, int jobID, string message)
		{
			var jobAlertSent = GetJobAlert(recipientID, jobID);
			if (jobAlertSent == null)
			{
				jobAlertSent = new JobNotification(recipientID, senderUserName, jobID);
			}

			_userServices.SendNotification(jobAlertSent, message);
		}

		#endregion

		#region Booking Services
	
		public Booking ReassignCarerForBooking(int clientID, int bookingID, int careWorkerID, string carerName)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;

			var booking = client.GetBooking(bookingID);
			if (booking == null) return null;

			var oldCarerID = booking.WorkerID;
			booking.ReassignCarerByClient(careWorkerID, carerName);
			// Send confirmation notification to old and new careworker
			if (oldCarerID != 0)
				SendBookingAlert(oldCarerID, client.DisplayName, booking.ID, " have reassigned this booking to someone else.");

			if (booking.WorkerID != 0)
				SendBookingAlert(booking.WorkerID, client.DisplayName, booking.ID, " have reassigned this booking to you.");

			return SaveBooking(booking);
		}

		public Booking AcknowledgeBookingChanges(int clientID, int bookingID)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;

			var booking = client.GetBooking(bookingID);
			if (booking == null) return null;

			// Send careworker notification changes have been accepted
			if (booking.WorkerID != 0)
				SendBookingAlert(booking.WorkerID, client.DisplayName, booking.ID, " have agreed to the new proposed booking changes");

			return SaveBooking(booking);
		}

		public Booking RescheduleBooking(int clientID, int bookingID, Duration newDuration)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;

			var booking = client.GetBooking(bookingID);
			if (booking == null) return null;

			booking.RescheduleByClient(newDuration);
			// Send confirmation notification to careworker

			if (booking.WorkerID != 0)
				SendBookingAlert(booking.WorkerID, client.DisplayName, booking.ID, " have agreed to the new proposed booking changes");


			return SaveBooking(booking);
		}

		public Booking CancelBooking(int clientID, int bookingID)
		{
			var client = RetrieveClient(clientID);
			if (client == null) return null;

			var booking = client.GetBooking(bookingID);
			if (booking == null) return null;

			booking.CancelByClient();
			// Notify careworker of cancelled booking

			if (booking.WorkerID != 0)
				SendBookingAlert(booking.WorkerID, client.DisplayName, booking.ID, " have cancelled the booking");

			return SaveBooking(booking);
		}

		private Booking SaveBooking(Booking booking)
		{
			this._entities.Update(booking);
			this._entities.Save();
			return booking;
		}

		private BookingNotification GetBookingAlert(int clientID, int bookingID)
		{
			return _userServices.RetrieveNotification<BookingNotification>(n => n.OwnerUserID == clientID && n.BookingID == bookingID);
		}

		private void SendBookingAlert(int recipientID, string senderUserName, int bookingID, string message)
		{
			var bookingAlertSent = GetBookingAlert(recipientID, bookingID);
			if (bookingAlertSent == null)
			{
				bookingAlertSent = new BookingNotification(recipientID, senderUserName, bookingID);
			}

			_userServices.SendNotification(bookingAlertSent, message);
		}

		#endregion

		#region Assign Rating

		public void AddRating(int clientID, RatingViewModel ratingData)
		{
			var client = this._entities.Single<Client>(a => a.ID == clientID);
			if (client == null)
				throw new ArgumentNullException("Client");
			var booking = this._entities.Single<Booking>(b => b.ID == ratingData.BookingID);

			Rating rating = _mapper.Map<RatingViewModel, Rating>(ratingData);
			rating.Status = RatingStatus.New;
			if (booking.AddRating(rating)) 
			{
				this._entities.Update(client);
				this._entities.Save();

				// send alert email to client
				//this._emailServices.SendBookingRatedEmail(booking, $"{client.FirstName} has rated for your a booking.");
			}
		}

		public void UpdateRating(int clientID, double oldRating, RatingViewModel ratingData)
		{
			var client = this._entities.Single<Client>(a => a.ID == clientID);
			if (client == null)
				throw new ArgumentNullException("Client");
			var booking = this._entities.Single<Booking>(b => b.ID == ratingData.BookingID);
			Rating rating = booking.Rating;
			rating = _mapper.Map<RatingViewModel, Rating>(ratingData, rating);
			rating.Status = RatingStatus.Update;

			if (booking.UpdateRating(rating))
			{
				this._entities.Update(client);
				this._entities.Save();

				// send alert email to client
				//this._emailServices.SendBookingRatingUpdatedEmail(booking, $"{client.FirstName} has updated a rating.");
			}
		}

		public RatingViewModel GetRatingVM(int clientID, int bookingID)
		{
			var client = this._entities.Single<Client>(a => a.ID == clientID);
			if (client == null)
				throw new ArgumentNullException("Client");
			var booking = this._entities.Single<Booking>(b => b.ID == bookingID);
			var rating = booking.Rating;
			var vm = new RatingViewModel();
			vm = _mapper.Map<Rating, RatingViewModel>(rating);
			return vm;
		}

		#endregion

	}
}