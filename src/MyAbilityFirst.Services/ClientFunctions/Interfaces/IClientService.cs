﻿using MyAbilityFirst.Domain;
using System.Collections.Generic;

namespace MyAbilityFirst.Services.ClientFunctions
{
	public interface IClientService
	{
		// Client Services
		Client RetrieveClient(int clientID);
		Client RetrieveClientByLoginID(string identityId);
		Client UpdateClient(Client updatedClient);

		// Patient Services
		Patient CreatePatient(int clientID, Patient patientData);
		Patient RetrievePatient(int clientID, int patientID);
		List<Patient> RetrieveAllPatients(int clientID);
		Patient UpdatePatient(int clientID, Patient patientData);

		// Contact Services
		Contact CreateContact(int clientID, int patientID, Contact contactData);
		Contact RetrieveContact(int clientID, int patientID, int contactID);
		List<Contact> RetrieveAllContacts(int clientID, int patientID);
		Contact UpdateContact(int clientID, int patientID, Contact contactData);
		bool DeleteContact(int clientID, int patientID, int contactID);
		List<Contact> ReplaceAllContacts(int clientID, int patientID, List<Contact> contacts);

		// Jobs
		Job DraftJob(int clientID, Job jobData);
		Job AdvertiseJob(int clientID, int jobID);

		// Ratings
		void AddRating(int clientID, RatingViewModel ratingData);
		void UpdateRating(int clientID, double oldRating, RatingViewModel ratingData);
		RatingViewModel GetRatingVM(int clientID, int bookingID);

	}
}