﻿using Autofac;

namespace MyAbilityFirst.Services.Common
{
	public class CommonModule : Autofac.Module
	{

		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			// register presentation layer service
			builder
				.RegisterType<PresentationService>()
				.As<IPresentationService>();

		}
	}
}