﻿using Mandrill;
using Mandrill.Models;
using Mandrill.Requests.Messages;
using System.Configuration;
using System.Threading.Tasks;

namespace Cenozoic.Infrastructure.Communication
{
	public class MandrillService : IEmailService
	{

		#region Fields

		#endregion

		#region Ctor

		public MandrillService()
		{
		}

		#endregion

		public void Send(string subject, string body, string address)
		{
			this.SendViaMandrill(subject, body, address);
		}

		#region Mandrill

		private void SendViaMandrill(string subject, string body, string toEmailAddress)
		{
			var email = new EmailMessage
			{
				Subject = subject,
				Html = body,
				FromEmail = ConfigurationManager.AppSettings["mandrill.From"],
				FromName = ConfigurationManager.AppSettings["mandrill.FromName"],
				To = new[] { new EmailAddress(toEmailAddress) }
			};

			var task = Task.Run(async () =>
			{
				// TODO: (Prod) Need to configure Mandrill to allow for the new domain name
				var mandrillApi = new MandrillApi(ConfigurationManager.AppSettings["mandrill.ApiKey"]);
				var n = await mandrillApi.SendMessage(new SendMessageRequest(email));
				return n;
			});

			// To get result of the call say, for error logging, uncomment these lines below
			// task.Wait();
			// var result = task.Result;
		}

		#endregion

	}
}