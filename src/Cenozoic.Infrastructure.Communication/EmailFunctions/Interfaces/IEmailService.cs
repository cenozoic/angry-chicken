﻿namespace Cenozoic.Infrastructure.Communication
{
	public interface IEmailService
	{
		void Send(string subject, string body, string address);
	}
}