﻿using Microsoft.AspNet.SignalR;

namespace Cenozoic.Infrastructure.Communication
{
	[Authorize]
	public class SignalRHub : Hub
	{
	}
}
