﻿using System.Configuration;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Cenozoic.Infrastructure.Communication
{
	public class TwilioService : ISMSService
	{
		public Task SendAsync(string toPhoneNumber, string message)
		{
			string accountID = ConfigurationManager.AppSettings["Twilio_SmsAccount_ID"];
			string authToken = ConfigurationManager.AppSettings["Twilio_SmsAccount_Token"];
			string fromPhoneNumber = ConfigurationManager.AppSettings["Twilio_SmsFrom_PhoneNumber"];

			// Initialize the Twilio client
			TwilioClient.Init(accountID, authToken);

			var result = MessageResource.Create(
				from: new PhoneNumber(fromPhoneNumber),
				to: new PhoneNumber(toPhoneNumber),
				body: message);
			return Task.FromResult(0);
		}
	}
}