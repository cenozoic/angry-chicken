﻿using System.Threading.Tasks;

namespace Cenozoic.Infrastructure.Communication
{
	public interface ISMSService
	{
		Task SendAsync(string toPhoneNumber, string message);
	}
}
