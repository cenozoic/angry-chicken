﻿using System.Web;

namespace Cenozoic.Infrastructure.Storage
{
	public interface IStorageService
	{
		string UploadToStorage(HttpPostedFileBase file, string containerName);
		bool DeleteFromStorage(string fileURL, string containerName);
	}
}