﻿using Autofac;

namespace Cenozoic.Infrastructure.Storage
{
	public class StorageModule : Autofac.Module
	{

		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			// register upload service
			builder
				.RegisterType<AzureBlobService>()
				.As<IStorageService>();
		}
	}
}