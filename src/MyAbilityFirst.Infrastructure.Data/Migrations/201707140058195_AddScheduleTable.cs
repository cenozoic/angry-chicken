namespace MyAbilityFirst.Infrastructure.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddScheduleTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Schedule",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        JobID = c.Int(nullable: false),
                        Duration_Start = c.DateTime(nullable: false),
                        Duration_End = c.DateTime(nullable: false),
                        ScheduleType = c.Int(nullable: false),
                        EffectiveEndDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Job", t => t.JobID, cascadeDelete: true)
                .Index(t => t.JobID);
        }
        
        public override void Down()
        {
            DropTable("dbo.Schedule");
        }
    }
}
