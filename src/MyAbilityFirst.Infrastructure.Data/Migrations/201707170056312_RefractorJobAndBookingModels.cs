namespace MyAbilityFirst.Infrastructure.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class RefractorJobAndBookingModels : DbMigration
    {
        public override void Up()
        {
						DropForeignKey("dbo.Booking", "ClientID", "dbo.Client");
				    DropIndex("dbo.Booking", new[] { "ClientID" });
						DropColumn("dbo.Booking", "ClientID");
						RenameColumn(table: "dbo.Booking", name: "Schedule_Start", newName: "Duration_Start");
						RenameColumn(table: "dbo.Booking", name: "Schedule_End", newName: "Duration_End");
						AlterColumn("dbo.Booking", "JobID", c => c.Int(nullable: false));
						AddColumn("dbo.Booking", "ScheduleID", c => c.Int(nullable: false));
						CreateIndex("dbo.Booking", "ScheduleID");
						AddForeignKey("dbo.Booking", "ScheduleID", "dbo.Schedule", "ID", cascadeDelete: true);

						DropIndex("dbo.Job", new[] { "ClientId" });
						CreateIndex("dbo.Job", "ClientID");
						AlterColumn("dbo.Job", "JobStatus", c => c.Int(nullable: false));
						DropColumn("dbo.Job", "GenderId");
						DropColumn("dbo.Job", "ServiceAt");
						DropColumn("dbo.Job", "PictureURL");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Job", "PictureURL", c => c.String());
            AddColumn("dbo.Job", "ServiceAt", c => c.DateTime());
            AddColumn("dbo.Job", "GenderId", c => c.Int(nullable: false));
						AlterColumn("dbo.Job", "JobStatus", c => c.String());
						DropIndex("dbo.Job", new[] { "ClientID" });
            CreateIndex("dbo.Job", "ClientId");

						DropForeignKey("dbo.Booking", "ScheduleID", "dbo.Schedule");
						DropIndex("dbo.Booking", new[] { "ScheduleID" });
						DropColumn("dbo.Booking", "ScheduleID");
						AlterColumn("dbo.Booking", "JobID", c => c.Int());
						RenameColumn(table: "dbo.Booking", name: "Duration_End", newName: "Schedule_End");
						RenameColumn(table: "dbo.Booking", name: "Duration_Start", newName: "Schedule_Start");
						AddColumn("dbo.Booking", "ClientID", c => c.Int(nullable: false));
						CreateIndex("dbo.Booking", "ClientID");
						AddForeignKey("dbo.Booking", "ClientID", "dbo.Client");
				}
    }
}
