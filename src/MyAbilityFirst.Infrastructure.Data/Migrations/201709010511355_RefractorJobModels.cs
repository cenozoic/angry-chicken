namespace MyAbilityFirst.Infrastructure.Data.Migrations
{
	using System.Data.Entity.Migrations;

	public partial class RefractorJobModels : DbMigration
	{
		public override void Up()
		{
			RenameIndex("dbo.Booking", "IX_CareWorkerID", "IX_WorkerID");
			RenameColumn(table: "dbo.Booking", name: "CareWorkerID", newName: "WorkerID");
			AddColumn("dbo.Booking", "ClientID", c => c.Int(nullable: false));
			CreateIndex("dbo.Booking", "ClientID");
			AddForeignKey("dbo.Booking", "ClientID", "dbo.Client", "ID");
			DropColumn("dbo.Booking", "Message");

			RenameColumn("dbo.Job", "PrimaryCarerID", "PrimaryWorkerID");
			RenameColumn("dbo.Job", "ClientId", "ClientID");
			DropForeignKey("dbo.Job", "ClientId", "dbo.Client");
			AddForeignKey("dbo.Job", "ClientID", "dbo.Client", "ID", cascadeDelete: true);
			CreateIndex("dbo.Job", "PrimaryWorkerID");

			RenameTable(name: "dbo.Application", newName: "Candidate");
			RenameColumn("dbo.Candidate", "ApplicantID", "UserID");
			DropForeignKey("dbo.Candidate", "ApplicantID", "dbo.CareWorker");
			AddForeignKey("dbo.Candidate", "UserID", "dbo.User", "ID", cascadeDelete: true);
			RenameIndex("dbo.Candidate", "IX_ApplicantID", "IX_UserID");
			AddColumn("dbo.Candidate", "BookingID", c => c.Int(nullable: false));
			CreateIndex("dbo.Candidate", "BookingID");
			AddForeignKey("dbo.Candidate", "BookingID", "dbo.Booking", "ID");
			AddColumn("dbo.Candidate", "Agreed", c => c.Boolean(nullable: false));

			RenameColumn("dbo.CaseNote", "OwnerUserID", "AuthorID");
			CreateIndex("dbo.CaseNote", "AuthorID");
			AddForeignKey("dbo.CaseNote", "AuthorID", "dbo.User", "ID", cascadeDelete: true);
		}

		public override void Down()
		{
			DropForeignKey("dbo.CaseNote", "AuthorID", "dbo.User");
			DropIndex("dbo.CaseNote", new[] { "AuthorID" });
			RenameColumn("dbo.CaseNote", "AuthorID", "OwnerUserID");

			DropColumn("dbo.Candidate", "Agreed");
			DropForeignKey("dbo.Candidate", "BookingID", "dbo.Booking");
			DropIndex("dbo.Candidate", new[] { "BookingID" });
			DropColumn("dbo.Candidate", "BookingID");
			RenameIndex("dbo.Candidate", "IX_UserID", "IX_ApplicantID");
			DropForeignKey("dbo.Candidate", "UserID", "dbo.User");
			RenameColumn("dbo.Candidate", "UserID", "ApplicantID");
			RenameTable(name: "dbo.Candidate", newName: "Application");

			DropIndex("dbo.Job", new[] { "PrimaryWorkerID" });
			DropForeignKey("dbo.Job", "ClientID", "dbo.Client");
			RenameColumn("dbo.Job", "ClientID", "ClientId");
			RenameColumn("dbo.Job", "PrimaryWorkerID", "PrimaryCarerID");

			AddColumn("dbo.Booking", "Message", c => c.String());
			DropForeignKey("dbo.Booking", "ClientID", "dbo.Client");
			DropIndex("dbo.Booking", new[] { "ClientID" });
			DropColumn("dbo.Booking", "ClientID");
			RenameColumn(table: "dbo.Booking", name: "WorkerID", newName: "CareWorkerID");
			RenameIndex("dbo.Booking", "IX_WorkerID", "IX_CareWorkerID");
		}
	}
}
