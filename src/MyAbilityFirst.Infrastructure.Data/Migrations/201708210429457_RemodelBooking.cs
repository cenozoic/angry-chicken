namespace MyAbilityFirst.Infrastructure.Data.Migrations
{
	using System.Data.Entity.Migrations;
	public partial class RemodelBooking : DbMigration
	{
		public override void Up()
		{
			DropColumn("dbo.Booking", "JobID");
		}
        
		public override void Down()
		{
			AddColumn("dbo.Boking", "JobID", c => c.Int(nullable: false));
		}
	}
}
