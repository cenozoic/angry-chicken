namespace MyAbilityFirst.Infrastructure.Data.Migrations
{
	using System.Data.Entity.Migrations;

	public partial class MoveNotificationSettingsToUserTable : DbMigration
	{
		public override void Up()
		{
			AddColumn("dbo.User", "NotificationSettings_ReceiveEmailNotifications", c => c.Boolean(nullable: false));
			AddColumn("dbo.User", "NotificationSettings_ReceiveSMSNotifications", c => c.Boolean(nullable: false));
			AddColumn("dbo.User", "NotificationSettings_ReceiveAppNotifications", c => c.Boolean(nullable: false));
			DropColumn("dbo.Client", "NotificationSettings_ReceiveEmailNotifications");
			DropColumn("dbo.Client", "NotificationSettings_ReceiveSMSNotifications");
		}

		public override void Down()
		{
			AddColumn("dbo.Client", "NotificationSettings_ReceiveSMSNotifications", c => c.Boolean(nullable: false));
			AddColumn("dbo.Client", "NotificationSettings_ReceiveEmailNotifications", c => c.Boolean(nullable: false));
			DropColumn("dbo.User", "NotificationSettings_ReceiveAppNotifications");
			DropColumn("dbo.User", "NotificationSettings_ReceiveSMSNotifications");
			DropColumn("dbo.User", "NotificationSettings_ReceiveEmailNotifications");
		}
	}
}
