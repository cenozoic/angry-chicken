namespace MyAbilityFirst.Infrastructure.Data.Migrations
{
	using System.Data.Entity.Migrations;

	public partial class RemodelJob : DbMigration
	{
		public override void Up()
		{
			DropIndex("dbo.Job", new[] { "ClientId" });
			CreateIndex("dbo.Job", "ClientID");
			DropColumn("dbo.Job", "ServiceId");
			DropColumn("dbo.Job", "PatientId");
			RenameColumn(table: "dbo.Job", name: "JobStatus", newName: "Status");
			AddColumn("dbo.Job", "PrimaryCarerID", c => c.Int(nullable: false));
			AddForeignKey("dbo.Job", "PrimaryCarerID", "dbo.CareWorker", "ID", cascadeDelete: true);
			AddColumn("dbo.Job", "LastAdvertised", c => c.DateTime());
		}

		public override void Down()
		{
			DropColumn("dbo.Job", "LastAdvertised");
			DropForeignKey("dbo.Job", "PrimaryCarerID", "dbo.CareWorker");
			DropColumn("dbo.Job", "PrimaryCarerID");
			RenameColumn(table: "dbo.Job", name: "Status", newName: "JobStatus");
			AddColumn("dbo.Job", "PatientId", c => c.Int(nullable: false));
			AddColumn("dbo.Job", "ServiceId", c => c.Int(nullable: false));
			CreateIndex("dbo.Job", "ClientId");
			DropIndex("dbo.Job", new[] { "ClientID" });
		}
	}
}
