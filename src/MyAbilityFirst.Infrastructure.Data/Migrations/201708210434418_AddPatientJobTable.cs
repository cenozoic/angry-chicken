namespace MyAbilityFirst.Infrastructure.Data.Migrations
{
	using System.Data.Entity.Migrations;
    
	public partial class AddPatientJobTable : DbMigration
	{
		public override void Up()
		{
			CreateTable(
				"dbo.PatientJob",
				c => new
						{
								Patient_ID = c.Int(nullable: false),
								Job_ID = c.Int(nullable: false),
						})
				.PrimaryKey(t => new { t.Patient_ID, t.Job_ID })
				.ForeignKey("dbo.Patient", t => t.Patient_ID, cascadeDelete: true)
				.ForeignKey("dbo.Job", t => t.Job_ID, cascadeDelete: true)
				.Index(t => t.Patient_ID)
				.Index(t => t.Job_ID);
		}
        
		public override void Down()
		{
			DropTable("dbo.PatientJob");
		}
	}
}
