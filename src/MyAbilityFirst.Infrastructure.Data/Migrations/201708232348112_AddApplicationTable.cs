namespace MyAbilityFirst.Infrastructure.Data.Migrations
{
	using System.Data.Entity.Migrations;

	public partial class AddApplicationTable : DbMigration
	{
		public override void Up()
		{
			CreateTable(
					"dbo.Application",
					c => new
					{
						ID = c.Int(nullable: false, identity: true),
						ApplicantID = c.Int(nullable: false),
						JobID = c.Int(nullable: false),
						CreatedAt = c.DateTime(nullable: false),
						UpdatedAt = c.DateTime(nullable: false),
					})
					.PrimaryKey(t => t.ID)
					.ForeignKey("dbo.CareWorker", t => t.ApplicantID)
					.ForeignKey("dbo.Job", t => t.JobID, cascadeDelete: true)
					.Index(t => t.ApplicantID)
					.Index(t => t.JobID);

		}

		public override void Down()
		{
			DropForeignKey("dbo.Application", "JobID", "dbo.Job");
			DropForeignKey("dbo.Application", "ApplicantID", "dbo.CareWorker");
			DropIndex("dbo.Application", new[] { "JobID" });
			DropIndex("dbo.Application", new[] { "ApplicantID" });
			DropTable("dbo.Application");
		}
	}
}
