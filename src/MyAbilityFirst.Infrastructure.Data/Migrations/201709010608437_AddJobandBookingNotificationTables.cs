namespace MyAbilityFirst.Infrastructure.Data.Migrations
{
	using System.Data.Entity.Migrations;

	public partial class AddJobandBookingNotificationTables : DbMigration
	{
		public override void Up()
		{

			CreateTable(
					"dbo.BookingNotification",
					c => new
					{
						ID = c.Int(nullable: false),
						BookingID = c.Int(nullable: false),
						AcceptedChange = c.Boolean(nullable: false),
					})
					.PrimaryKey(t => t.ID)
					.ForeignKey("dbo.Notification", t => t.ID)
					.Index(t => t.ID);

			CreateTable(
					"dbo.JobNotification",
					c => new
					{
						ID = c.Int(nullable: false),
						JobID = c.Int(nullable: false),
						AcceptedChange = c.Boolean(nullable: false),
					})
					.PrimaryKey(t => t.ID)
					.ForeignKey("dbo.Notification", t => t.ID)
					.ForeignKey("dbo.Job", t => t.JobID, cascadeDelete: true)
					.Index(t => t.ID)
					.Index(t => t.JobID);
		}

		public override void Down()
		{
			DropTable("dbo.JobNotification");
			DropTable("dbo.BookingNotification");
		}
	}
}
