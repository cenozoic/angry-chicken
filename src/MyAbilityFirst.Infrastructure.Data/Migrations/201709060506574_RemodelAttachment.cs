namespace MyAbilityFirst.Infrastructure.Data.Migrations
{
	using System.Data.Entity.Migrations;

	public partial class RemodelAttachment : DbMigration
	{
		public override void Up()
		{
			CreateTable(
					"dbo.Attachment",
					c => new
					{
						ID = c.Int(nullable: false, identity: true),
						OwnerUserID = c.Int(nullable: false),
						URL = c.String(),
						Type = c.Int(nullable: false),
					})
					.PrimaryKey(t => t.ID)
					.ForeignKey("dbo.User", t => t.OwnerUserID, cascadeDelete: true)
					.Index(t => t.OwnerUserID);

			DropTable("dbo.UserAttachment");
		}

		public override void Down()
		{
			CreateTable(
					"dbo.UserAttachment",
					c => new
					{
						ID = c.Int(nullable: false, identity: true),
						UserID = c.Int(nullable: false),
						URL = c.String(),
						AttachmentTypeID = c.Int(nullable: false),
					})
					.PrimaryKey(t => t.ID);

			DropForeignKey("dbo.Attachment", "OwnerUserID", "dbo.User");
			DropIndex("dbo.Attachment", new[] { "OwnerUserID" });
			DropTable("dbo.Attachment");
		}
	}
}
