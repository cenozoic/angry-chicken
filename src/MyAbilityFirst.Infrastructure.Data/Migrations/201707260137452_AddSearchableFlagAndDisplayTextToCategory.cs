namespace MyAbilityFirst.Infrastructure.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddSearchableFlagAndDisplayTextToCategory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Category", "Searchable", c => c.Boolean(nullable: false));
            AddColumn("dbo.Category", "DisplayText", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Category", "DisplayText");
            DropColumn("dbo.Category", "Searchable");
        }
    }
}
