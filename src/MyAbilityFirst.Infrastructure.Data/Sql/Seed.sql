-- ***************************************************************************************************************************
-- Seed Data
-- ***************************************************************************************************************************
IF EXISTS (SELECT 1 from sys.procedures (nolock) where name = 'sproc_AddCategory') 
	drop procedure  [dbo].[sproc_AddCategory]
GO
	CREATE PROCEDURE [dbo].[sproc_AddCategory]
		@CategoryName [nvarchar](max), @Searchable [bit]
	AS
	BEGIN
		if not exists (select 1 from category where name = @CategoryName)	
			insert into Category(Name) values (@CategoryName)	
	END
	GO


IF EXISTS (SELECT 1 from sys.procedures (nolock) where name = 'sproc_AddSubCategory') 
	drop procedure  [dbo].[sproc_AddSubCategory]
GO
	CREATE PROCEDURE [dbo].[sproc_AddSubCategory]
		@SubCategoryName [nvarchar](max), @CategoryID [int]
	AS
	BEGIN
		if not exists (select 1 from Subcategory where name = @SubCategoryName) 
			insert into Subcategory values (@CategoryID, @SubCategoryName) 
	END
	GO




declare @categoryNameSeed as varchar(255) 
declare @categoryIDSeed as int

set @categoryNameSeed = 'Gender'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Male', @categoryIDSeed
		exec sproc_AddSubCategory 'Female', @categoryIDSeed
		exec sproc_AddSubCategory 'Other', @categoryIDSeed
		exec sproc_AddSubCategory 'None / Not Disclosed', @categoryIDSeed

set @categoryNameSeed = 'ClientMarketingInfo'
exec sproc_AddCategory @categoryNameSeed, 0
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Existing Client', @categoryIDSeed
		exec sproc_AddSubCategory 'Previous Client', @categoryIDSeed
		exec sproc_AddSubCategory 'Search Engine', @categoryIDSeed
		exec sproc_AddSubCategory 'Social Media', @categoryIDSeed
		exec sproc_AddSubCategory 'Forum or Blog', @categoryIDSeed
		exec sproc_AddSubCategory 'TV', @categoryIDSeed
		exec sproc_AddSubCategory 'Newspaper', @categoryIDSeed
		exec sproc_AddSubCategory 'Magazine', @categoryIDSeed
		exec sproc_AddSubCategory 'Radio', @categoryIDSeed
		exec sproc_AddSubCategory 'Friend / Family', @categoryIDSeed
		exec sproc_AddSubCategory 'Event', @categoryIDSeed
		exec sproc_AddSubCategory 'Flyer or Card', @categoryIDSeed
		exec sproc_AddSubCategory 'Business / Supplier', @categoryIDSeed
		exec sproc_AddSubCategory 'Case Manager', @categoryIDSeed
		exec sproc_AddSubCategory 'Hospital Staff', @categoryIDSeed
		exec sproc_AddSubCategory 'NDIS', @categoryIDSeed
		exec sproc_AddSubCategory 'School', @categoryIDSeed
		exec sproc_AddSubCategory 'Doctor / Medical Practitioner', @categoryIDSeed
		exec sproc_AddSubCategory 'Care / Support Worker', @categoryIDSeed
		exec sproc_AddSubCategory 'Other', @categoryIDSeed

set @categoryNameSeed = 'CareType'
exec sproc_AddCategory @categoryNameSeed, 0
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Disability Support', @categoryIDSeed
		exec sproc_AddSubCategory 'Aged Care', @categoryIDSeed
		exec sproc_AddSubCategory 'Mental Health', @categoryIDSeed
		exec sproc_AddSubCategory 'Post-Surgery', @categoryIDSeed
		exec sproc_AddSubCategory 'Other', @categoryIDSeed

set @categoryNameSeed = 'CareWorkerMarketingInfo'
exec sproc_AddCategory @categoryNameSeed, 0
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Existing Care Worker', @categoryIDSeed
		exec sproc_AddSubCategory 'Previous Care Worker', @categoryIDSeed
		exec sproc_AddSubCategory 'Search Engine', @categoryIDSeed
		exec sproc_AddSubCategory 'Social Media', @categoryIDSeed
		exec sproc_AddSubCategory 'Forum or Blog', @categoryIDSeed
		exec sproc_AddSubCategory 'TV', @categoryIDSeed
		exec sproc_AddSubCategory 'Newspaper', @categoryIDSeed
		exec sproc_AddSubCategory 'Magazine', @categoryIDSeed
		exec sproc_AddSubCategory 'Radio', @categoryIDSeed
		exec sproc_AddSubCategory 'Friend / Family', @categoryIDSeed
		exec sproc_AddSubCategory 'Event', @categoryIDSeed
		exec sproc_AddSubCategory 'Flyer or Card', @categoryIDSeed
		exec sproc_AddSubCategory 'Seek', @categoryIDSeed
		exec sproc_AddSubCategory 'LinkedIn', @categoryIDSeed
		exec sproc_AddSubCategory 'CareerOne', @categoryIDSeed
		exec sproc_AddSubCategory 'Blue Steps', @categoryIDSeed
		exec sproc_AddSubCategory 'Gumtree', @categoryIDSeed
		exec sproc_AddSubCategory 'Indeed', @categoryIDSeed
		exec sproc_AddSubCategory 'Other Job Website', @categoryIDSeed
		exec sproc_AddSubCategory 'Colleague', @categoryIDSeed
		exec sproc_AddSubCategory 'Teacher', @categoryIDSeed
		exec sproc_AddSubCategory 'Careers Advisor', @categoryIDSeed
		exec sproc_AddSubCategory 'Other', @categoryIDSeed


set @categoryNameSeed = 'QualificationType'
exec sproc_AddCategory @categoryNameSeed, 0
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Nursing Services', @categoryIDSeed
		exec sproc_AddSubCategory 'Personal Care', @categoryIDSeed
		exec sproc_AddSubCategory 'Social & Domestic Assistance', @categoryIDSeed

set @categoryNameSeed = 'NursingQualifications'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Enrolled Nurse', @categoryIDSeed
		exec sproc_AddSubCategory 'Registered Nurse', @categoryIDSeed
		exec sproc_AddSubCategory 'One or more years of nursing experience', @categoryIDSeed
		exec sproc_AddSubCategory 'Registeration number', @categoryIDSeed

set @categoryNameSeed = 'PersonalCareQualifications'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Certificate 3 Individual Support', @categoryIDSeed
		exec sproc_AddSubCategory 'Certificate 4 Individual support', @categoryIDSeed
		exec sproc_AddSubCategory 'Certificate 3 Aged Care', @categoryIDSeed
		exec sproc_AddSubCategory 'Certificate 4 Aged Care', @categoryIDSeed
		exec sproc_AddSubCategory 'Certificate 3 in Disabilities', @categoryIDSeed
		exec sproc_AddSubCategory 'Certificate 4 in Disabilities', @categoryIDSeed
		exec sproc_AddSubCategory 'Degree in Nursing', @categoryIDSeed
		exec sproc_AddSubCategory 'Degree in allied Health', @categoryIDSeed
		exec sproc_AddSubCategory 'Certificate 3 in Home and Community Care', @categoryIDSeed
		exec sproc_AddSubCategory 'Certificate 4 in Home and Community Care', @categoryIDSeed
		exec sproc_AddSubCategory 'Assistant in Nursing (AIN)', @categoryIDSeed
		exec sproc_AddSubCategory 'Other Relevant Qualification', @categoryIDSeed
		exec sproc_AddSubCategory 'Working Toward Degree in Nursing', @categoryIDSeed
		exec sproc_AddSubCategory '2 or more Years of Experience', @categoryIDSeed
		exec sproc_AddSubCategory '5 or more Years of Experience', @categoryIDSeed
		exec sproc_AddSubCategory '10 or more Years of Experience', @categoryIDSeed

set @categoryNameSeed = 'OtherQualifications'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Current First Aid (Level 1)', @categoryIDSeed
		exec sproc_AddSubCategory 'Current Senior First Aid (Level 2)', @categoryIDSeed
		exec sproc_AddSubCategory 'Palliative Care', @categoryIDSeed
		exec sproc_AddSubCategory 'Manual Handling Certified', @categoryIDSeed
		exec sproc_AddSubCategory 'Dementia Care', @categoryIDSeed
		exec sproc_AddSubCategory 'Medication Assistance', @categoryIDSeed
		exec sproc_AddSubCategory 'Food Handling', @categoryIDSeed
		exec sproc_AddSubCategory 'LGBTI Health alliance Inclusive Training', @categoryIDSeed
		exec sproc_AddSubCategory 'Hoist', @categoryIDSeed
		exec sproc_AddSubCategory 'PEG Feeding', @categoryIDSeed
		exec sproc_AddSubCategory 'Bowel and Bladder Management', @categoryIDSeed
		exec sproc_AddSubCategory 'Water Safety', @categoryIDSeed
		exec sproc_AddSubCategory 'Certificate 3 Community Services', @categoryIDSeed
		exec sproc_AddSubCategory 'Certificate 4 Community Services', @categoryIDSeed
		exec sproc_AddSubCategory 'Degree in Psychology', @categoryIDSeed
		exec sproc_AddSubCategory 'Degree in Social Work', @categoryIDSeed
		exec sproc_AddSubCategory 'Valid Driverís License', @categoryIDSeed
		exec sproc_AddSubCategory 'Registered Vehicle', @categoryIDSeed


set @categoryNameSeed = 'VerificationChecks'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Passport', @categoryIDSeed
		exec sproc_AddSubCategory 'National Police Criminal Records Check', @categoryIDSeed
		exec sproc_AddSubCategory 'Working with Children Check (WWCC)', @categoryIDSeed
		exec sproc_AddSubCategory 'Working with Vulnerable People Check (WWVPC)', @categoryIDSeed



set @categoryNameSeed = 'FormalEducation'
exec sproc_AddCategory @categoryNameSeed, 0
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Completed Year 9-11', @categoryIDSeed
		exec sproc_AddSubCategory 'Completed Year 12', @categoryIDSeed
		exec sproc_AddSubCategory 'Diploma', @categoryIDSeed
		exec sproc_AddSubCategory 'TAFE/Trade Certificate', @categoryIDSeed
		exec sproc_AddSubCategory 'University Undergraduate', @categoryIDSeed
		exec sproc_AddSubCategory 'Post Graduate Degree', @categoryIDSeed
		exec sproc_AddSubCategory 'Masters', @categoryIDSeed
		exec sproc_AddSubCategory 'PhD', @categoryIDSeed




set @categoryNameSeed = 'ServiceSubType'
exec sproc_AddCategory @categoryNameSeed, 0
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Activities, Outings and Community Access', @categoryIDSeed
		exec sproc_AddSubCategory 'Assistance with Eating', @categoryIDSeed
		exec sproc_AddSubCategory 'Assistance with Ventilator', @categoryIDSeed
		exec sproc_AddSubCategory 'Assist with Bowel and Bladder Management', @categoryIDSeed
		exec sproc_AddSubCategory 'Assist with Self Medication', @categoryIDSeed
		exec sproc_AddSubCategory 'Bowel and Bladder Management', @categoryIDSeed
		exec sproc_AddSubCategory 'Care Assessment, Planning, Co-ordination', @categoryIDSeed
		exec sproc_AddSubCategory 'Case Assessment and Management', @categoryIDSeed
		exec sproc_AddSubCategory 'Catheter Care', @categoryIDSeed
		exec sproc_AddSubCategory 'Cleaning and Laundry', @categoryIDSeed
		exec sproc_AddSubCategory 'Companionship', @categoryIDSeed
		exec sproc_AddSubCategory 'Continence Assessment and Management', @categoryIDSeed
		exec sproc_AddSubCategory 'Exercise Assistance', @categoryIDSeed
		exec sproc_AddSubCategory 'Hoist and Transfer', @categoryIDSeed
		exec sproc_AddSubCategory 'Home Maintenance', @categoryIDSeed
		exec sproc_AddSubCategory 'Life skills Development', @categoryIDSeed
		exec sproc_AddSubCategory 'Lifestyle Co-ordinator', @categoryIDSeed
		exec sproc_AddSubCategory 'Light Gardening', @categoryIDSeed
		exec sproc_AddSubCategory 'Light Housework', @categoryIDSeed
		exec sproc_AddSubCategory 'Light Massage', @categoryIDSeed
		exec sproc_AddSubCategory 'Manual Handling (Getting in and out of Bed)', @categoryIDSeed
		exec sproc_AddSubCategory 'Meal Preparation', @categoryIDSeed
		exec sproc_AddSubCategory 'Medication Management', @categoryIDSeed
		exec sproc_AddSubCategory 'Nursing Services', @categoryIDSeed
		exec sproc_AddSubCategory 'Palliative Care', @categoryIDSeed
		exec sproc_AddSubCategory 'Palliative Nursing Care', @categoryIDSeed
		exec sproc_AddSubCategory 'PEG Feeding', @categoryIDSeed
		exec sproc_AddSubCategory 'Personal Assistant (Administration)', @categoryIDSeed
		exec sproc_AddSubCategory 'Pre and Post-Acute Hospital Care', @categoryIDSeed
		exec sproc_AddSubCategory 'Respiratory Care', @categoryIDSeed
		exec sproc_AddSubCategory 'Shopping', @categoryIDSeed
		exec sproc_AddSubCategory 'Showering, Dressing, Grooming', @categoryIDSeed
		exec sproc_AddSubCategory 'Sports and Exercise', @categoryIDSeed
		exec sproc_AddSubCategory 'Toileting', @categoryIDSeed
		exec sproc_AddSubCategory 'Transport', @categoryIDSeed
		exec sproc_AddSubCategory 'Vital Signs Monitoring', @categoryIDSeed
		exec sproc_AddSubCategory 'Wound Care', @categoryIDSeed

set @categoryNameSeed = 'OtherPreference'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'LGBTI Friendly', @categoryIDSeed
		exec sproc_AddSubCategory 'Pet Friendly', @categoryIDSeed
		exec sproc_AddSubCategory 'Non-Smoker', @categoryIDSeed

set @categoryNameSeed = 'DisabilityCareExperience'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Acquired Brain Injury', @categoryIDSeed
		exec sproc_AddSubCategory 'Autism', @categoryIDSeed
		exec sproc_AddSubCategory 'Cerebral Palsy', @categoryIDSeed
		exec sproc_AddSubCategory 'Cystic Fibrosis', @categoryIDSeed
		exec sproc_AddSubCategory 'Down Syndrome', @categoryIDSeed
		exec sproc_AddSubCategory 'Epilepsy', @categoryIDSeed
		exec sproc_AddSubCategory 'Hearing Impairment', @categoryIDSeed
		exec sproc_AddSubCategory 'Intellectual Disabilities', @categoryIDSeed
		exec sproc_AddSubCategory 'Motor Neuron Disease', @categoryIDSeed
		exec sproc_AddSubCategory 'Muscular Dystrophy', @categoryIDSeed
		exec sproc_AddSubCategory 'Physical Disabilities', @categoryIDSeed
		exec sproc_AddSubCategory 'Spina Bifida', @categoryIDSeed
		exec sproc_AddSubCategory 'Spinal Cord Injury', @categoryIDSeed
		exec sproc_AddSubCategory 'Vision Impairment', @categoryIDSeed

set @categoryNameSeed = 'MentalHealthCareExperience'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Anxiety', @categoryIDSeed
		exec sproc_AddSubCategory 'Bipolar Disorder', @categoryIDSeed
		exec sproc_AddSubCategory 'Depression', @categoryIDSeed
		exec sproc_AddSubCategory 'Eating Disorders', @categoryIDSeed
		exec sproc_AddSubCategory 'Hoarding', @categoryIDSeed
		exec sproc_AddSubCategory 'Obsessive Compulsive Disorder (OCD)', @categoryIDSeed
		exec sproc_AddSubCategory 'Post-Traumatic Stress Disorder (PTSD)', @categoryIDSeed
		exec sproc_AddSubCategory 'Schizophrenia', @categoryIDSeed
		exec sproc_AddSubCategory 'Substance Abuse and Addiction', @categoryIDSeed

set @categoryNameSeed = 'AgedCareExperience'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Dementia', @categoryIDSeed
		exec sproc_AddSubCategory 'Parkinsonís Disease', @categoryIDSeed

set @categoryNameSeed = 'ChronicMedicalConditionsCareExperience'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Arthritis', @categoryIDSeed
		exec sproc_AddSubCategory 'Asthma', @categoryIDSeed
		exec sproc_AddSubCategory 'Cardiovascular Disease', @categoryIDSeed
		exec sproc_AddSubCategory 'COPD or Respiratory Illness', @categoryIDSeed
		exec sproc_AddSubCategory 'Diabetes', @categoryIDSeed

set @categoryNameSeed = 'Personality'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Extroverted, outgoing and energetic', @categoryIDSeed
		exec sproc_AddSubCategory 'Calm and relaxed', @categoryIDSeed
		exec sproc_AddSubCategory 'Introverted, quiet and reserved', @categoryIDSeed

set @categoryNameSeed = 'Relationship'
exec sproc_AddCategory @categoryNameSeed, 0
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Partner / Spouse', @categoryIDSeed
		exec sproc_AddSubCategory 'Immediate Family', @categoryIDSeed
		exec sproc_AddSubCategory 'Extended Family', @categoryIDSeed
		exec sproc_AddSubCategory 'Legal Representative/ Advocate', @categoryIDSeed
		exec sproc_AddSubCategory 'Approved Provider', @categoryIDSeed
		exec sproc_AddSubCategory 'Care Co-ordinator', @categoryIDSeed
		exec sproc_AddSubCategory 'Friend / Neighbour', @categoryIDSeed

set @categoryNameSeed = 'Interest'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Cooking', @categoryIDSeed
		exec sproc_AddSubCategory 'Gardening', @categoryIDSeed
		exec sproc_AddSubCategory 'Indoor Games / Puzzles', @categoryIDSeed
		exec sproc_AddSubCategory 'Movies', @categoryIDSeed
		exec sproc_AddSubCategory 'Music', @categoryIDSeed
		exec sproc_AddSubCategory 'Cultural Festivals', @categoryIDSeed
		exec sproc_AddSubCategory 'Pets', @categoryIDSeed
		exec sproc_AddSubCategory 'Photography / Art', @categoryIDSeed
		exec sproc_AddSubCategory 'Reading', @categoryIDSeed
		exec sproc_AddSubCategory 'Sports', @categoryIDSeed
		exec sproc_AddSubCategory 'Travel', @categoryIDSeed
		exec sproc_AddSubCategory 'Working', @categoryIDSeed
		exec sproc_AddSubCategory 'Other', @categoryIDSeed

set @categoryNameSeed = 'Language'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Arabic', @categoryIDSeed
		exec sproc_AddSubCategory 'Cantonese', @categoryIDSeed
		exec sproc_AddSubCategory 'Croatian', @categoryIDSeed
		exec sproc_AddSubCategory 'English', @categoryIDSeed
		exec sproc_AddSubCategory 'French', @categoryIDSeed
		exec sproc_AddSubCategory 'German', @categoryIDSeed
		exec sproc_AddSubCategory 'Greek', @categoryIDSeed
		exec sproc_AddSubCategory 'Hebrew', @categoryIDSeed
		exec sproc_AddSubCategory 'Hindi', @categoryIDSeed
		exec sproc_AddSubCategory 'Hungarian', @categoryIDSeed
		exec sproc_AddSubCategory 'Indonesian', @categoryIDSeed
		exec sproc_AddSubCategory 'Italian', @categoryIDSeed
		exec sproc_AddSubCategory 'Japanese', @categoryIDSeed
		exec sproc_AddSubCategory 'Korean', @categoryIDSeed
		exec sproc_AddSubCategory 'Mandarin', @categoryIDSeed
		exec sproc_AddSubCategory 'Maltese', @categoryIDSeed
		exec sproc_AddSubCategory 'Macedonian', @categoryIDSeed
		exec sproc_AddSubCategory 'Netherlandic ', @categoryIDSeed
		exec sproc_AddSubCategory 'Persian', @categoryIDSeed
		exec sproc_AddSubCategory 'Polish', @categoryIDSeed
		exec sproc_AddSubCategory 'Portugese ', @categoryIDSeed
		exec sproc_AddSubCategory 'Russian', @categoryIDSeed
		exec sproc_AddSubCategory 'Serbian', @categoryIDSeed
		exec sproc_AddSubCategory 'Sinhalese', @categoryIDSeed
		exec sproc_AddSubCategory 'Samoan ', @categoryIDSeed
		exec sproc_AddSubCategory 'Spanish', @categoryIDSeed
		exec sproc_AddSubCategory 'Tamil', @categoryIDSeed
		exec sproc_AddSubCategory 'Tagalog (Filipino)', @categoryIDSeed
		exec sproc_AddSubCategory 'Turkish', @categoryIDSeed
		exec sproc_AddSubCategory 'Vietnamese', @categoryIDSeed
		exec sproc_AddSubCategory 'Auslan (Australian sign Language)', @categoryIDSeed
		exec sproc_AddSubCategory 'Other', @categoryIDSeed

set @categoryNameSeed = 'Culture'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Australian', @categoryIDSeed
		exec sproc_AddSubCategory 'Australian Aboriginal', @categoryIDSeed
		exec sproc_AddSubCategory 'Australian South East Islander', @categoryIDSeed
		exec sproc_AddSubCategory 'Torres Strait Islander', @categoryIDSeed
		exec sproc_AddSubCategory 'New Zealander', @categoryIDSeed
		exec sproc_AddSubCategory 'Maori', @categoryIDSeed
		exec sproc_AddSubCategory 'Polynesian', @categoryIDSeed
		exec sproc_AddSubCategory 'Other Oceanian', @categoryIDSeed
		exec sproc_AddSubCategory 'Western European', @categoryIDSeed
		exec sproc_AddSubCategory 'Northern European', @categoryIDSeed
		exec sproc_AddSubCategory 'Southern & Eastern European', @categoryIDSeed
		exec sproc_AddSubCategory 'Middle Eastern', @categoryIDSeed
		exec sproc_AddSubCategory 'Jewish', @categoryIDSeed
		exec sproc_AddSubCategory 'Asian', @categoryIDSeed
		exec sproc_AddSubCategory 'North American', @categoryIDSeed
		exec sproc_AddSubCategory 'South American', @categoryIDSeed
		exec sproc_AddSubCategory 'Central American', @categoryIDSeed
		exec sproc_AddSubCategory 'Caribbean Islander', @categoryIDSeed
		exec sproc_AddSubCategory 'South African', @categoryIDSeed
		exec sproc_AddSubCategory 'Other African', @categoryIDSeed
		exec sproc_AddSubCategory 'Other', @categoryIDSeed

set @categoryNameSeed = 'Religion'
exec sproc_AddCategory @categoryNameSeed, 1
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Anglican', @categoryIDSeed
		exec sproc_AddSubCategory 'Buddhist', @categoryIDSeed
		exec sproc_AddSubCategory 'Catholic', @categoryIDSeed
		exec sproc_AddSubCategory 'Christian', @categoryIDSeed
		exec sproc_AddSubCategory 'Greek Orthodox', @categoryIDSeed
		exec sproc_AddSubCategory 'Hindu', @categoryIDSeed
		exec sproc_AddSubCategory 'Islamic', @categoryIDSeed
		exec sproc_AddSubCategory 'Jewish', @categoryIDSeed
		exec sproc_AddSubCategory 'Presbyterian', @categoryIDSeed
		exec sproc_AddSubCategory 'Russian Orthodox', @categoryIDSeed
		exec sproc_AddSubCategory 'Sikh', @categoryIDSeed
		exec sproc_AddSubCategory 'Spiritual', @categoryIDSeed
		exec sproc_AddSubCategory 'Uniting Church ', @categoryIDSeed
		exec sproc_AddSubCategory 'Other', @categoryIDSeed
		exec sproc_AddSubCategory 'None', @categoryIDSeed

set @categoryNameSeed = 'Pet'
exec sproc_AddCategory @categoryNameSeed, 0
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Dog', @categoryIDSeed
		exec sproc_AddSubCategory 'Cat', @categoryIDSeed
		exec sproc_AddSubCategory 'Other', @categoryIDSeed

set @categoryNameSeed = 'MedicalLivingNeed'
exec sproc_AddCategory @categoryNameSeed, 0
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Cardiac / Vascular', @categoryIDSeed
		exec sproc_AddSubCategory 'Diabetes / Cholesterol', @categoryIDSeed
		exec sproc_AddSubCategory 'Incontinence (Bladder or Bowel)', @categoryIDSeed
		exec sproc_AddSubCategory 'Memory Loss / Dementia', @categoryIDSeed
		exec sproc_AddSubCategory 'Nutrition / Hydration', @categoryIDSeed
		exec sproc_AddSubCategory 'Other Barriers or Concerns', @categoryIDSeed
		exec sproc_AddSubCategory 'Physical / Mobility', @categoryIDSeed
		exec sproc_AddSubCategory 'Complex communication ', @categoryIDSeed
		exec sproc_AddSubCategory 'Psychological / Behavioral ', @categoryIDSeed
		exec sproc_AddSubCategory 'Skin Integrity / Wound Management', @categoryIDSeed
		exec sproc_AddSubCategory 'Speech / Swallowing', @categoryIDSeed

set @categoryNameSeed = 'JobService'
exec sproc_AddCategory @categoryNameSeed, 0
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
		exec sproc_AddSubCategory 'Companionship & Social Support', @categoryIDSeed
		exec sproc_AddSubCategory 'Cleaning & Laundry', @categoryIDSeed
		exec sproc_AddSubCategory 'Community Participation, Sports & Activities', @categoryIDSeed
		exec sproc_AddSubCategory 'Showering, Toileting & Dressing', @categoryIDSeed
		exec sproc_AddSubCategory 'Transport', @categoryIDSeed
		exec sproc_AddSubCategory 'Manual Transfer & Mobility', @categoryIDSeed
		exec sproc_AddSubCategory 'Independent Living Skills', @categoryIDSeed
		exec sproc_AddSubCategory 'Assist with Self Medication', @categoryIDSeed
		exec sproc_AddSubCategory 'Meal Preparation & Shopping ', @categoryIDSeed
		exec sproc_AddSubCategory 'Nursing Services', @categoryIDSeed

set @categoryNameSeed = 'Attachment'
exec sproc_AddCategory @categoryNameSeed, 0
	select @categoryIDSeed = ID from Category where Name = @categoryNameSeed
	 exec sproc_AddSubCategory 'Photo', @categoryIDSeed
	 exec sproc_AddSubCategory 'Care Plan', @categoryIDSeed
	 exec sproc_AddSubCategory 'NDIS approved Plan', @categoryIDSeed
	 exec sproc_AddSubCategory 'GP Documents', @categoryIDSeed
 	 exec sproc_AddSubCategory 'Birth Certificate', @categoryIDSeed
	 exec sproc_AddSubCategory 'Medicare', @categoryIDSeed
	 exec sproc_AddSubCategory 'Proof of Age', @categoryIDSeed
	 exec sproc_AddSubCategory 'Psychology Report', @categoryIDSeed
	 exec sproc_AddSubCategory 'Review and Assessment Reports', @categoryIDSeed

-- Organisations
declare @orgName as varchar(255)
set @orgName = 'CPL' if not exists (select 1 from Organisation where name = @orgName)
	begin insert into Organisation (Name, LogoURL, Address, Phone, Introduction )	values (@orgName,'Images/'+@orgName+'.jpg','','','') end
set @orgName = 'Cootharinga' if not exists (select 1 from Organisation where name = @orgName)
	begin insert into Organisation (Name, LogoURL, Address, Phone, Introduction )	values (@orgName,'Images/'+@orgName+'.jpg','','','') end
set @orgName = 'Multicap' if not exists (select 1 from Organisation where name = @orgName)
	begin insert into Organisation (Name, LogoURL, Address, Phone, Introduction )	values (@orgName,'Images/'+@orgName+'.jpg','','','') end

-- Searchable categories, will appear in autocomplete
update category set searchable = 1 where Name in (
	'Gender','VerificationChecks', 'OtherPreference', 
	'NursingQualifications', 'PersonalCareQualifications',
	'OtherQualifications', 'DisabilityCareExperience',
	'MentalHealthCareExperience', 'AgedCareExperience',
	'ChronicMedicalConditionsCareExperience', 'Language',
	'Culture', 'Religion', 'Interest', 'Personality'
);

-- AutoComplete display text
update category set DisplayText = 'Gender' where Name = 'Gender';
update category set DisplayText = 'Background Checks' where Name = 'VerificationChecks';
update category set DisplayText = 'Other Preferences' where Name = 'OtherPreference';
update category set DisplayText = 'Nursing' where Name = 'NursingQualifications';
update category set DisplayText = 'Personal Care ' where Name = 'PersonalCareQualifications'
update category set DisplayText = 'Social and Domestic Care' where Name = 'OtherQualifications'
update category set DisplayText = 'Disability Care ' where Name = 'DisabilityCareExperience'
update category set DisplayText = 'Mental Health' where Name = 'MentalHealthCareExperience'
update category set DisplayText = 'Aged Care' where Name = 'AgedCareExperience'
update category set DisplayText = 'Chronic Conditions' where Name = 'ChronicMedicalConditionsCareExperience'
update category set DisplayText = 'Language' where Name = 'Language'
update category set DisplayText = 'Culture' where Name = 'Culture' 
update category set DisplayText = 'Religion' where Name = 'Religion' 
update category set DisplayText = 'Interest' where Name = 'Interest' 
update category set DisplayText = 'Personality' where Name = 'Personality'