﻿namespace Cenozoic.Models
{
	public class Subcategory
	{
		public int ID { get; set; }
		public int CategoryID { get; set; }
		public string Name { get; set; }
	}
}