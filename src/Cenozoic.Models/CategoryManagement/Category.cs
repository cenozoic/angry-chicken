﻿namespace Cenozoic.Models
{
	public class Category
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public bool Searchable { get; set; }
		public string DisplayText { get; set; }
	}
}