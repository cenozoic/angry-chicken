﻿namespace Cenozoic.Models
{
	public class AutoCompleteViewModel
	{
		public string SubcategoryName;
		public string CategoryName;
		public int SubcategoryID;
		public string LocationName;
		public string PlaceID;
	}
}
