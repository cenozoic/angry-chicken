﻿namespace Cenozoic.Models
{
	public enum UserStatus
	{
		Registered, 
		Active, 
		Inactive, 
		Suspended, 
		Deleted
	}
}
