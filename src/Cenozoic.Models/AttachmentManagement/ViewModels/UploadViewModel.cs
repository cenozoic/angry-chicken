﻿using System.Web;

namespace Cenozoic.Models
{
	public class UploadViewModel
	{
		public AttachmentType AttachmentType { get; set; }
		public HttpPostedFileBase File { get; set; }
	}
}
