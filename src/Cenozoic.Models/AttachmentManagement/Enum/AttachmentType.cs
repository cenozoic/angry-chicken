﻿namespace Cenozoic.Models
{
	public enum AttachmentType
	{
		ProfilePhoto,
		CarePlanDocument,
		NdisPlanDocument,
		GpDocument,
		BirthCertificate,
		MedicareDocument,
		ProofOfAgeDocument,
		PsychologyReport,
		ReviewAssessmentReport
	}
}