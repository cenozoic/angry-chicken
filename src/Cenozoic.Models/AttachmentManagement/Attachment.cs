﻿using System;

namespace Cenozoic.Models
{
	public class Attachment
	{

		#region Properties

		public int ID { get; private set; }
		public int OwnerUserID { get; private set; }
		public string URL { get; protected set; }
		public AttachmentType Type { get; protected set; }

		#endregion

		#region Ctor

		protected Attachment()
		{
			// required by EF
		}

		public Attachment(int userID, AttachmentType type)
		{
			this.OwnerUserID = userID;
			this.Type = type;
		}

		#endregion

		#region Helpers

		public void RemoveUrl() => this.URL = string.Empty;

		public void SetUrl(string url)
		{
			if (string.IsNullOrEmpty(url) || url.Equals(this.URL, StringComparison.OrdinalIgnoreCase))
				return;

			this.URL = url;
		}

		public string GetFriendlyName()
		{
			switch (this.Type)
			{
				case AttachmentType.ProfilePhoto:
					return "Profile Photo";
				case AttachmentType.CarePlanDocument:
					return "Care Plan Document";
				case AttachmentType.NdisPlanDocument:
					return "Ndis Plan Document";
				case AttachmentType.GpDocument:
					return "GpDocument";
				case AttachmentType.BirthCertificate:
					return "Birth Certificate";
				case AttachmentType.MedicareDocument:
					return "Medicare Document";
				case AttachmentType.ProofOfAgeDocument:
					return "Proof Of Age Document";
				case AttachmentType.PsychologyReport:
					return "Psychology Report";
				case AttachmentType.ReviewAssessmentReport:
					return "Review Assessment Report";
				default:
					return "";
			}
		}

		#endregion

	}
}