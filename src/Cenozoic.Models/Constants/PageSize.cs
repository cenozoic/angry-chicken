﻿namespace Cenozoic.Models
{
	public class PageSize
	{

		public const int DefaultSearchPageSize = 10;
		public const int MaxSearchPageSize = 20;

		public const int DefaultMessagePageSize = 20;
		public const int MaxMessagePageSize = 50;

	}
}
