﻿namespace Cenozoic.Models
{
	public class NotificationSettings
	{
		public bool ReceiveEmailNotifications { get; set; }
		public bool ReceiveSMSNotifications { get; set; }
		public bool ReceiveAppNotifications { get; set; }

		public NotificationSettings()
		{
			this.ReceiveAppNotifications = true;
			this.ReceiveSMSNotifications = true;
			this.ReceiveEmailNotifications = true;
		}
	}
}