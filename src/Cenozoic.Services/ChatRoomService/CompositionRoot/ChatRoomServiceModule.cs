﻿using Autofac;

namespace Cenozoic.Services
{
	public class ChatRoomServiceModule : Autofac.Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			// register Chat Services
			builder
					.RegisterType<ChatRoomService>()
					.As<IChatRoomService>();
		}
	}
}
