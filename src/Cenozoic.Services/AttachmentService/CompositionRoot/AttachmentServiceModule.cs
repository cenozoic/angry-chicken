﻿using Autofac;

namespace Cenozoic.Services
{
	public class AttachmentServiceModule : Module
	{

		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			// register AttachmentService
			builder
					.RegisterType<AttachmentService>()
					.AsImplementedInterfaces();

		}
	}
}