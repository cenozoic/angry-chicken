﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cenozoic.Models;
using Cenozoic.Infrastructure;
using Cenozoic.Infrastructure.Storage;


namespace Cenozoic.Services
{
	public class AttachmentService : IAttachmentService
	{

		#region Fields

		private readonly IWriteEntities _entities;
		private readonly IStorageService _storageServices;

		#endregion

		#region Ctor

		public AttachmentService(IWriteEntities entities, IStorageService storageServices)
		{
			this._entities = entities;
			this._storageServices = storageServices;
		}

		#endregion

		#region Helpers

		public Attachment CreateAttachment(int ownerUserID, AttachmentType type, HttpPostedFileBase file)
		{
			if (ownerUserID < 1 || file == null)
				return null;

			// Create Attachment and attempt to upload
			Attachment attachment = new Attachment(ownerUserID, type);
			if (!uploadFile(ref attachment, file))
				return null;

			this._entities.Create(attachment);
			this._entities.Save();
			return attachment;
		}

		public Attachment RetrieveAttachment(int attachmentID)
		{
			return this._entities.Get<Attachment>(a => a.ID == attachmentID).FirstOrDefault();
		}

		public List<Attachment> RetrieveAllAttachments(int userID)
		{
			return this._entities.Get<Attachment>(a => a.OwnerUserID == userID).ToList();
		}

		public Attachment UpdateAttachment(int attachmentID, HttpPostedFileBase file)
		{
			// load existing attachment
			Attachment attachment = RetrieveAttachment(attachmentID);
			if (attachment == null || file == null)
				return null;

			// delete file from server if exists
			if (!removeFile(ref attachment))
				return null;

			// update content on server and new URL
			if (!uploadFile(ref attachment, file))
				return null;

			this._entities.Update(attachment);
			this._entities.Save();
			return attachment;
		}

		public bool DeleteAttachment(int attachmentID)
		{
			// load existing attachment
			Attachment attachment = RetrieveAttachment(attachmentID);
			if (attachment == null || string.IsNullOrWhiteSpace(attachment.URL))
				return false;

			// delete file from server if exists
			if (!removeFile(ref attachment))
				return false;

			this._entities.Update(attachment);
			this._entities.Save();
			return true;
		}

		#endregion


		#region helpers

		private bool uploadFile(ref Attachment attachment, HttpPostedFileBase file)
		{
			var newUrl = this._storageServices.UploadToStorage(file, attachment.Type.ToString().ToLower());
			if (newUrl.Count() < 0)
				return false;

			attachment.SetUrl(newUrl);
			return true;
		}

		private bool removeFile(ref Attachment attachment)
		{ 
			var successfullyRemovedFromServer = !string.IsNullOrWhiteSpace(attachment.URL) && this._storageServices.DeleteFromStorage(attachment.URL, attachment.Type.ToString().ToLower());
			if (successfullyRemovedFromServer)
				attachment.RemoveUrl();
			return successfullyRemovedFromServer;
		}

		#endregion

	}
}