﻿using Cenozoic.Models;
using System.Collections.Generic;
using System.Web;

namespace Cenozoic.Services
{
	public interface IAttachmentService
	{
		Attachment CreateAttachment(int ownerUserID, AttachmentType type, HttpPostedFileBase file);
		Attachment RetrieveAttachment(int attachmentID);
		List<Attachment> RetrieveAllAttachments(int userID);
		Attachment UpdateAttachment(int attachmentID, HttpPostedFileBase file);
		bool DeleteAttachment(int attachmentID);
	}
}