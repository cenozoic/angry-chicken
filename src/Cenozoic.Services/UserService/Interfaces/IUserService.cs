﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Cenozoic.Models;

namespace Cenozoic.Services
{
	public interface IUserService
	{
		// User Services
		void CreateUser<UserType>(string loginIdentityID, string email) where UserType : User;
		User GetUser(int id);
		User GetLoggedInUser();
		string GetUserName(int id);
		string GetLoggedInUserType();
		bool EmailVerified(string loginIdentityID);

		// Shortlist Services
		Shortlist CreateShortlist(int ownerUserID, Shortlist shortlistData);
		Shortlist RetrieveShortlistBySelectedUserID(int ownerUserID, int selectedUserID);
		List<Shortlist> RetrieveAllShortlists(int ownerUserID);
		Shortlist UpdateShortlist(int ownerUserID, Shortlist shortlistData);

		// Category Services
		UserSubcategory CreateUserSubcategory(int ownerUserID, UserSubcategory userSubcategoryData);
		UserSubcategory RetrieveUserSubcategoryBySubcategoryID(int ownerUserID, int subcategoryID);
		List<UserSubcategory> RetrieveAllUserSubcategories(int ownerUserID);
		UserSubcategory UpdateUserSubcategory(int ownerUserID, UserSubcategory userSubcategoryData);
		List<UserSubcategory> ReplaceAllUserSubCategories(int ownerUserID, int[] postedSubCategoryIDs, List<UserSubcategory> customValueList);

		// Notification Services
		NotificationType CreateNotification<NotificationType>(NotificationType notificationData) where NotificationType : Notification;
		NotificationType RetrieveNotification<NotificationType>(Expression<Func<NotificationType, bool>> filter) where NotificationType : Notification;
		NotificationType UpdateNotification<NotificationType>(NotificationType notificationData) where NotificationType : Notification;
		NotificationType SetNotificationAsRead<NotificationType>(NotificationType notificationData) where NotificationType : Notification;
		NotificationType SetNotificationAsClosed<NotificationType>(NotificationType notificationData) where NotificationType : Notification;
		List<NotificationType> RetrieveOpenNotifications<NotificationType>(int ownerUserID) where NotificationType : Notification;

		void SendNotification<NotificationType>(NotificationType notification, string notificationHtmlContent) where NotificationType : Notification;
		void SendEmail(int recipientID, string subject, string content);
		void SendSMS(int recipientID, string content);
	}
}