﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Cenozoic.Models;
using Cenozoic.Infrastructure;
using Cenozoic.Infrastructure.Auth;
using Cenozoic.Infrastructure.Communication;

namespace Cenozoic.Services
{
	public class UserService : IUserService
	{

		#region Fields

		private readonly IWriteEntities _entities;
		private readonly IMapper _mapper;
		private readonly IBroadcastService _hub;
		private readonly IEmailService _emailServices;
		private readonly ILoginService _loginServices;
		private readonly ISMSService _smsServices;

		#endregion

		#region Ctor

		public UserService(IWriteEntities entities, IMapper mapper, IBroadcastService hub, ILoginService loginServices, IEmailService emailServices, ISMSService smsServices)
		{
			this._entities = entities;
			this._mapper = mapper;
			this._hub = hub;
			this._loginServices = loginServices;
			this._emailServices = emailServices;
			this._smsServices = smsServices;
		}

		#endregion

		#region User Services

		public void CreateUser<UserType>(string loginIdentityID, string email) where UserType : User
		{
			var now = DateTime.Now;
			var newUser = (UserType) Activator.CreateInstance(typeof(UserType), loginIdentityID);
			newUser.CreatedAt = now;
			newUser.UpdatedAt = now;
			newUser.Email = email;
			newUser.Status = UserStatus.Registered;
			this._entities.Create(newUser);
			this._entities.Save();
		}

		public User GetUser(int id)
		{
			var users = this._entities.Get<User>(u => u.ID == id);
			if (users.Any())
			{
				return users.First();
			}
			return null;
		}

		public User GetLoggedInUser()
		{
			var loginID = _loginServices.GetCurrentLoginIdentityID();
			var users = this._entities.Get<User>(u => u.LoginIdentityId == loginID);
			if (users.Any())
			{
				return users.First();
			}
			return null;
		}

		public string GetUserName(int id)
		{
			var users = this._entities.Get<User>(u => u.ID == id);
			if (users.Any())
				return _loginServices.GetLoginUserName(users.First().LoginIdentityId);
			return null;
		}

		public string GetLoggedInUserType()
		{
			return _loginServices.GetUserType(_loginServices.GetCurrentLoginIdentityID());
		}
		
		public bool EmailVerified(string loginIdentityID)
		{
			return _loginServices.EmailVerified(loginIdentityID);
		}

		#endregion

		#region Shortlist

		public Shortlist CreateShortlist(int ownerUserID, Shortlist shortlistData)
		{
			User user = GetUser(ownerUserID);
			if (user != null)
			{
				user.AddNewShortlist(shortlistData);

				_entities.Update(user);
				_entities.Save();
				return shortlistData;
			}
			return null;
		}

		public Shortlist RetrieveShortlistBySelectedUserID(int ownerUserID, int selectedUserID)
		{
			User user = GetUser(ownerUserID);
			if (user != null)
			{
				return user.GetExistingShortlist(selectedUserID);
			}
			return null;
		}

		public List<Shortlist> RetrieveAllShortlists(int ownerUserID)
		{
			User user = GetUser(ownerUserID);
			if (user != null)
			{
				return user.Shortlists.ToList();
			}
			return null;
		}

		public Shortlist UpdateShortlist(int ownerUserID, Shortlist shortlistData)
		{
			User user = GetUser(ownerUserID);
			if (user != null)
			{
				user.UpdateExistingShortlist(shortlistData);

				this._entities.Update(user);
				_entities.Save();
				return shortlistData;
			}
			return null;
		}

		#endregion
		
		#region UserSubcategory

		public UserSubcategory CreateUserSubcategory(int ownerUserID, UserSubcategory userSubcategoryData)
		{
			User user = GetUser(ownerUserID);
			if (user != null)
			{
				user.AddNewUserSubCategory(userSubcategoryData);

				_entities.Update(user);
				_entities.Save();
				return userSubcategoryData;
			}
			return null;
		}

		public UserSubcategory RetrieveUserSubcategoryBySubcategoryID(int ownerUserID, int subcategoryID)
		{
			User user = GetUser(ownerUserID);
			if (user != null)
			{
				return user.GetExistingUserSubcategory(subcategoryID);
			}
			return null;
		}

		public List<UserSubcategory> RetrieveAllUserSubcategories(int ownerUserID)
		{
			User user = GetUser(ownerUserID);
			if (user != null)
			{
				return user.GetAllExistingUserSubcategories();
			}
			return null;
		}

		public UserSubcategory UpdateUserSubcategory(int ownerUserID, UserSubcategory userSubcategoryData)
		{
			User user = GetUser(ownerUserID);
			if (user != null)
			{
				user.UpdateExistingUserSubcategory(userSubcategoryData);

				this._entities.Update(user);
				_entities.Save();
				return userSubcategoryData;
			}
			return null;
		}



		public List<UserSubcategory> ReplaceAllUserSubCategories(int ownerUserID, int[] postedSubCategoryIDs, List<UserSubcategory> customValueList)
		{
			List<UserSubcategory> existingSubcategoryList = RetrieveAllUserSubcategories(ownerUserID) ?? new List<UserSubcategory>();
			int[] previousSubCategoryIDs = existingSubcategoryList.Select(x => x.SubCategoryID).ToArray();
			postedSubCategoryIDs = postedSubCategoryIDs ?? new int[0];

			// add new
			IEnumerable<int> actionableIDs = postedSubCategoryIDs.Except(previousSubCategoryIDs);
			IEnumerable<UserSubcategory> actionableObjects = getNewUSCObjects(ownerUserID, actionableIDs, customValueList);
			foreach (UserSubcategory usc in actionableObjects)
			{
				CreateUserSubcategory(ownerUserID, usc);
			}

			// flag existing as selected
			actionableObjects = getExistingUSCObjects(existingSubcategoryList, postedSubCategoryIDs, customValueList, true);
			foreach (UserSubcategory usc in actionableObjects)
			{
				UpdateUserSubcategory(ownerUserID, usc);
			}

			// flag existing as unselected
			actionableIDs = previousSubCategoryIDs.Except(postedSubCategoryIDs);
			actionableObjects = getExistingUSCObjects(existingSubcategoryList, actionableIDs, customValueList, false);
			foreach (UserSubcategory usc in actionableObjects)
			{
				UpdateUserSubcategory(ownerUserID, usc);
			}
			return RetrieveAllUserSubcategories(ownerUserID);
		}

		#endregion

		#region Notification
		
		public NotificationType CreateNotification<NotificationType>(NotificationType notificationData) where NotificationType : Notification
		{
			User user = GetUser(notificationData.OwnerUserID);
			if (user != null)
			{
				user.AddNotification(notificationData);

				_entities.Update(user);
				_entities.Save();

				return notificationData;
			}
			return null;
		}

		public NotificationType RetrieveNotification<NotificationType>(Expression<Func<NotificationType, bool>> filter) where NotificationType : Notification
		{
			return this._entities.Single(filter);
		}

		public NotificationType UpdateNotification<NotificationType>(NotificationType notificationData) where NotificationType : Notification
		{
			User user = GetUser(notificationData.OwnerUserID);
			if (user != null)
			{
				user.UpdateNotification(notificationData);

				this._entities.Update(user);
				_entities.Save();
				return notificationData;
			}
			return null;
		}

		public NotificationType SetNotificationAsRead<NotificationType>(NotificationType notificationData) where NotificationType : Notification
		{
			notificationData.SetAsRead();
			return UpdateNotification(notificationData);
		}

		public NotificationType SetNotificationAsClosed<NotificationType>(NotificationType notificationData) where NotificationType : Notification
		{
			notificationData.Closed = true;
			return UpdateNotification(notificationData);
		}

		public List<NotificationType> RetrieveOpenNotifications<NotificationType>(int ownerUserID) where NotificationType : Notification
		{
			User user = GetUser(ownerUserID);
			if (user != null)
				return _entities.Get<NotificationType>(n => n.OwnerUserID == ownerUserID && !n.Closed).ToList();
			return null;
		}

		public void SendNotification<NotificationType>(NotificationType notification, string notificationHtmlContent) where NotificationType : Notification
		{
			var user = GetUser(notification.OwnerUserID);
			if (user != null && user.NotificationSettings.ReceiveAppNotifications)
			{
				var userlist = new List<string>();
				userlist.Add(GetUserName(notification.OwnerUserID));
				_hub.SendAlert(userlist, notificationHtmlContent, notification.ID);
			}
		}

		public void SendEmail(int recipientID, string subject, string content)
		{
			var user = GetUser(recipientID);
			if (user.NotificationSettings.ReceiveEmailNotifications && user.Email.Contains("@")) // TODO: proper email validation
			{
				_emailServices.Send(subject, content, user.Email);
			}
		}
		
		public void SendSMS(int recipientID, string content)
		{
			var user = GetUser(recipientID);
			if (user.NotificationSettings.ReceiveSMSNotifications && user.Phone != "")
			{
				_smsServices.SendAsync(user.Phone, content);
			}
		}
		
		#endregion

		#region Helpers

		private IEnumerable<UserSubcategory> getExistingUSCObjects(IEnumerable<UserSubcategory> existingSubcategoryList, IEnumerable<int> selectedSubcategoryIDs, List<UserSubcategory> customValueList, bool isSelected)
		{
			return (
				from usc in existingSubcategoryList
				join scID in selectedSubcategoryIDs on usc.SubCategoryID equals scID
				select usc
			)
			.Select(usc => {
				usc.Selected = isSelected;
				if (customValueList.Exists((cvl => cvl.OwnerUserID == usc.OwnerUserID && cvl.SubCategoryID == usc.SubCategoryID)))
					usc.CustomValue = customValueList.Single(cvl => cvl.OwnerUserID == usc.OwnerUserID && cvl.SubCategoryID == usc.SubCategoryID).CustomValue;
				return usc;
			}).ToList();
		}

		private IEnumerable<UserSubcategory> getNewUSCObjects(int ownerUserID, IEnumerable<int> selectedSubcategoryIDs, List<UserSubcategory> customValueList)
		{
			return selectedSubcategoryIDs.Select(scID =>
			{
				UserSubcategory usc = new UserSubcategory();
				usc.OwnerUserID = ownerUserID;
				usc.Selected = true;
				usc.SubCategoryID = scID;
				if (customValueList.Exists((cvl => cvl.OwnerUserID == usc.OwnerUserID && cvl.SubCategoryID == usc.SubCategoryID)))
					usc.CustomValue = customValueList.Single(cvl => cvl.OwnerUserID == usc.OwnerUserID && cvl.SubCategoryID == usc.SubCategoryID).CustomValue;
				return usc;
			}).ToList();
		}

		#endregion

	}
}
