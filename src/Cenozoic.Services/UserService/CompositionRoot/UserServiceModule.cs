﻿using Autofac;

namespace Cenozoic.Services
{
	public class UserServiceModule : Autofac.Module
	{

		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			// register UserService
			builder
				.RegisterType<UserService>()
				.As<IUserService>();
		}
	}
}