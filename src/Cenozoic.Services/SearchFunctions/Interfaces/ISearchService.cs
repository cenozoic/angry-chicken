﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cenozoic.Models;


namespace Cenozoic.Services
{
	public interface ISearchService
	{
		List<Subcategory> GetSearchableSubCategories(string searchString);
		Task<List<PlaceAutoComplete>> GetSearchableLocations(string searchString);
		Task<PlaceDetails> GetLatLngFromPlaceID(string placeID);
	}
}
