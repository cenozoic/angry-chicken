﻿using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Cenozoic.Models;
using Cenozoic.Infrastructure;
using System.Configuration;

namespace Cenozoic.Services
{
	public class SearchService : ISearchService
	{

		#region Fields

		protected readonly IWriteEntities _entities;
		private readonly ICacheService _cacheServices;

		#endregion

		#region Ctor

		public SearchService(IWriteEntities entities, ICacheService cacheServices)
		{
			this._entities = entities;
			this._cacheServices = cacheServices;
		}

		#endregion

		#region Subcategory Autocomplete

		public List<Subcategory> GetSearchableSubCategories(string searchString)
		{
			return (
				from sc in this._entities.GetLazy<Subcategory>(sc => sc.Name.Contains(searchString))
				join c in this._entities.GetLazy<Category>(c => c.Searchable) on sc.CategoryID equals c.ID
				select sc
			).ToList();
		}

		#endregion

		#region Google Autocomplete and Place API

		public async Task<List<PlaceAutoComplete>> GetSearchableLocations(string searchString)
		{ // Cache results for 24 hours
			return await _cacheServices.GetOrAddAsync(searchString, () => this.GetGoogleAutoComplete(searchString), DateTimeOffset.Now.AddDays(1), null, null);
		}

		public async Task<PlaceDetails> GetLatLngFromPlaceID(string placeID)
		{ // Cache results for 24 hours
			return await _cacheServices.GetOrAdd(placeID, () => this.GetGooglePlace(placeID), DateTimeOffset.Now.AddDays(1), null, null);
		}

		#endregion
		
		#region User filters

		protected PagedList<T> GetResultsAsPagedList<T>(IQueryable<T> query, int pageNumber = 1, int pageSize = PageSize.DefaultSearchPageSize)
		{
			if (query == null)
				return null;

			if (pageNumber < 1)
				pageNumber = 1;

			if (pageSize < 1)
				pageSize = PageSize.DefaultSearchPageSize;

			if (pageSize > PageSize.MaxSearchPageSize)
				pageSize = PageSize.MaxSearchPageSize;

			return new PagedList<T>(query, (int)pageNumber, (int)pageSize);
		}

		protected IQueryable<TUser> GetAllUsers<TUser>() where TUser: User
		{
			return _entities.GetLazy<TUser>();
		}

		protected IQueryable<TUser> FilterUserByName<TUser>(IQueryable<TUser> query, string name) where TUser : User
		{
			return query.Where(u => u.FirstName.Contains(name) || u.LastName.Contains(name) || name.Contains(u.FirstName + " " + u.LastName));
		}

		protected IQueryable<TUser> FilterUserByDistance<TUser>(IQueryable<TUser> query, decimal? longitude, decimal? latitude, float? radiusInKm) where TUser : User
		{
			return query.Where(u => DbGeography.FromText("POINT(" + longitude.ToString() + " " + latitude.ToString() + ")", 4326).Distance(u.Address.GeoPoint) / 1000 <= radiusInKm);
		}

		protected IQueryable<TUser> FilterUserByLatLngBounds<TUser>(IQueryable<TUser> query, decimal? minLong, decimal? minLat, decimal? maxLong, decimal? maxLat) where TUser : User
		{
			return query.Where(u => 
				DbGeography.FromText("POLYGON((" 
					+ minLong.ToString() + " " + minLat.ToString() + ", " + minLong.ToString() + " " + maxLat.ToString() + ", " 
					+ maxLong.ToString() + " " + maxLat.ToString() + ", " + maxLong.ToString() + " " + minLat.ToString() + ", "
					+ minLong.ToString() + " " + minLat.ToString() + "))" , 4326).Intersects(u.Address.GeoPoint) && minLong != maxLong && minLat != maxLat);
		}

		protected IQueryable<TUser> FilterUserByUserSubcategory<TUser>(IQueryable<TUser> query, int[] subcategoryIDs) where TUser : User
		{
			if (subcategoryIDs == null || subcategoryIDs.Count() == 0)
				return null;
			return getUserIDsFromUserSubcategory(query, subcategoryIDs);
		}

		#endregion

		#region Helpers

		protected int[] GetPostedIDsAndRemoveFromArray(string categoryName, ref int[] postedSubcategoryIDs)
		{
			postedSubcategoryIDs = postedSubcategoryIDs ?? new int[0];
			int[] res = postedSubcategoryIDs.Intersect(getSubCategoryList(categoryName).Select(sc => sc.ID).ToArray()).ToArray();
			postedSubcategoryIDs = postedSubcategoryIDs.Except(res).ToArray();
			return res;
		}

		private IQueryable<TUser> getUserIDsFromUserSubcategory<TUser>(IQueryable<TUser> query, int[] subcategoryIDs) where TUser: User
		{
			return
				from q in query
				join userIDs in (
						from usc in _entities.GetLazy<UserSubcategory>()
						join sc in subcategoryIDs
							on usc.SubCategoryID equals sc
						group usc.OwnerUserID by usc.OwnerUserID into grp
						where grp.Count() == subcategoryIDs.Count()
						select grp.Key
				)
					on q.ID equals userIDs
				select q;
		}
				
		private async Task<List<PlaceAutoComplete>> GetGoogleAutoComplete(string input)
		{
			var http = new HttpClient();
			var url = String.Format("https://maps.googleapis.com/maps/api/place/autocomplete/json?input={0}&types=(regions)&components=country:au&key=" + ConfigurationManager.AppSettings["GoogleMapsAPIKey"], input);
			var response = await http.GetAsync(url);
			var jsonResponseString = await response.Content.ReadAsStringAsync();

			return JsonConvert.DeserializeObject<PlaceAutoCompleteRootobject>(jsonResponseString).predictions.Select(p =>
			{
				PlaceAutoComplete pac = new PlaceAutoComplete();
				pac.description = p.description;
				pac.place_id = p.place_id;
				return pac;
			}).ToList();
		}

		private async Task<PlaceDetails> GetGooglePlace(string placeID)
		{
			var http = new HttpClient();
			var url = String.Format("https://maps.googleapis.com/maps/api/place/details/json?placeid={0}&key=" + ConfigurationManager.AppSettings["GoogleMapsAPIKey"], placeID);
			var response = await http.GetAsync(url);
			var jsonResponseString = await response.Content.ReadAsStringAsync();

			PlaceDetailsRootobject jsonRoot = JsonConvert.DeserializeObject<PlaceDetailsRootobject>(jsonResponseString);
			PlaceDetails pd = new PlaceDetails();
			pd.lat = jsonRoot.result.geometry.location.lat;
			pd.lng = jsonRoot.result.geometry.location.lng;

			return pd;
		}

		private List<Subcategory> getSubCategoryList(string categoryName)
		{
			return getSubCategory(getCategory(categoryName).ID);
		}

		private Category getCategory(string categoryName)
		{
			return this._entities.Get<Category>(
			cc => cc.Name.Equals(categoryName),
			null).First();
		}

		private List<Subcategory> getSubCategory(int categoryID)
		{
			return this._entities.Get<Subcategory>(
			 sc => sc.CategoryID == categoryID,
			 o => o.OrderBy(sc => sc.ID)).ToList();
		}

		#endregion

	}
}
