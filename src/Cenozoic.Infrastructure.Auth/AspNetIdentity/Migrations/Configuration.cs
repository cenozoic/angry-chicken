namespace Cenozoic.Infrastructure.Auth.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Cenozoic.Infrastructure.Auth.EntityFramework.AspNetIdentityDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Cenozoic.Infrastructure.Auth.EntityFramework.AspNetIdentityDbContext";
        }

        protected override void Seed(Cenozoic.Infrastructure.Auth.EntityFramework.AspNetIdentityDbContext context)
        {
        }
    }
}
