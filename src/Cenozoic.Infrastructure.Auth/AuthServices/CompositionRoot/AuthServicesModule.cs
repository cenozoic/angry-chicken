﻿using Autofac;

namespace Cenozoic.Infrastructure.Auth
{
	public class AuthServicesModule : Autofac.Module
	{

		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			// register LoginService
			builder
					.RegisterType<AspNetLoginService>()
					.As<ILoginService>();
		}
	}
}