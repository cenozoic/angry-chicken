﻿using System.Threading.Tasks;

namespace Cenozoic.Infrastructure.Auth
{
	public interface ILoginService
	{
		Task SignOn();
		Task SignOut();
		string GetCurrentLoginIdentityID();
		bool EmailVerified(string loginIdentityID);
		string GetCurrentLoginUserName();
		string GetUserType(string loginIdentityID);
		bool IsAuthenticated();
		string GetLoginUserName(string loginIdentityID);
	}
}