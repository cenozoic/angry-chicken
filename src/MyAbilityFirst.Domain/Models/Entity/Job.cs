using System;
using System.Collections.Generic;
using System.Linq;
using Cenozoic.Models;

namespace MyAbilityFirst.Domain
{
	public class Job
	{

		#region Properties

		public int ID { get; set; }
		public int ClientID { get; private set; }
		public int PrimaryWorkerID { get; set; }
		public Address Address { get; set; }

		public string Title { get; set; }
		public string Description { get; set; }
		public int GenderID { get; set; }
		public JobStatus Status { get; set; }
		public virtual ICollection<Patient> Participants { get; set; }

		public virtual ICollection<Schedule> Schedules { get; set; }
		public virtual ICollection<Candidate> Applications { get; set; }
		public virtual ICollection<JobNotification> JobNotifications { get; set; }

		public DateTime? CreatedAt { get; set; }
		public DateTime? UpdatedAt { get; set; }
		public DateTime? LastAdvertised { get; set; }
		
		private bool CanBeModified => !(this.Status != JobStatus.Cancelled || this.Status == JobStatus.Expired);
		private bool CanAcceptApplicant => (this.CanBeModified && this.PrimaryWorkerID == 0);
		private bool NotYetFinalized => (
			this.Schedules.Count == 0 || this.PrimaryWorkerID == 0 || this.Status == JobStatus.PendingCoordinatorAction
		);
		public bool FirstBookingAboutToStart => (
			(from s in Schedules from d in s.GetScheduledDurations(DateTime.Now, DateTime.Now.AddDays(1))
			where(d.Start - DateTime.Now).TotalHours <= DeadLine.HoursBeforeBookingStartsBuffer select d).Count() > 0 
		);
		private double HoursPassedSinceLastAdvertised => ((DateTime.Now - (DateTime)this.LastAdvertised).TotalHours);

		#endregion

		#region Ctor

		protected Job()
		{
			// required by EF
			this.Address = new Address();
			this.Schedules = new List<Schedule>();
			this.Participants = new List<Patient>();
		}

		public Job(int clientID, Address address, List<Schedule> schedules, List<Patient> participants)
		{
			this.ClientID = clientID;
			this.Address = address;
			this.Status = JobStatus.Draft;
			this.Schedules = schedules ?? new List<Schedule>();
			this.Participants = participants ?? new List<Patient>();
			this.CreatedAt = DateTime.Now;
			this.UpdatedAt = DateTime.Now;
		}

		#endregion

		#region Client transitions 

		public bool AdvertiseByClient()
		{
			return this.Advertise();
		}

		public bool AssignPrimaryCarerByClient(int careWorkerID)
		{
			return this.AssignPrimaryWorker(careWorkerID);
		}

		public bool DismissPrimaryCarerByClient()
		{
			return this.DismissPrimaryWorker();
		}

		public bool NewScheduleByClient(Schedule scheduleData)
		{
			return this.AddSchedule(scheduleData);
		}

		public bool RescheduleByClient(Schedule scheduleData)
		{ 
			return this.Reschedule(scheduleData);
		}

		public bool CancelByClient()
		{
			return this.Cancel();
		}

		#endregion


		#region Carer transitions

		public bool ApplyByCarer(int careWorkerID)
		{
			return this.Apply(careWorkerID);
		}

		public bool WithdrawApplicationByCarer(int careWorkerID)
		{
			return this.WithdrawApplication(careWorkerID);
		}

		public bool NewScheduleByCarer(Schedule scheduleData)
		{
				return this.AddSchedule(scheduleData);
		}

		public bool RescheduleByCarer(Schedule scheduleData)
		{
			return this.Reschedule(scheduleData);
		}

		public bool ResignByCarer()
		{
			return this.PrimaryWorkerResign();
		}

		#endregion


		#region Scheduler transitions

		public bool FlagForCoordinatorActionByScheduler()
		{
			if (NotYetFinalized) 
			{
				// Advertised for X or more hours with no response, or Bookings about to start in X hours
				if (this.HoursPassedSinceLastAdvertised >= DeadLine.HoursBeforeCoordinatorEscalation || this.FirstBookingAboutToStart)
				{
					this.Status = JobStatus.PendingCoordinatorAction;
					this.UpdatedAt = DateTime.Now;
					return true;
				}
			}
			return false;
		}

		public bool FinalizeByScheduler()
		{ // finalize all changes in absent of user confirmation, after X hours from first advertised
			if (this.NotYetFinalized && HoursPassedSinceLastAdvertised >= DeadLine.HoursBeforeJobChangesAutomaticallyFinalize)
			{
				this.Status = this.Schedules.Count > 0 && this.PrimaryWorkerID != 0 ? JobStatus.Active : JobStatus.Cancelled;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}
		
		public bool ExpireByScheduler()
		{ // all schedules' effective end date has passed
			if (this.Schedules.Count(sch => sch.EffectiveEndDate < DateTime.Now) == this.Schedules.Count) 
			{
				this.Status = JobStatus.Expired;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		#endregion


		#region Coordinator transitions

		public bool AssignPrimaryCarerByCoordinator(int careWorkerID)
		{
			return this.AssignPrimaryWorker(careWorkerID);
		}

		public bool NewScheduleByCoordinator(Schedule scheduleData)
		{
			return this.AddSchedule(scheduleData);
		}

		public bool RescheduleByCoordinator(Schedule scheduleData)
		{
			return this.Reschedule(scheduleData);
		}

		#endregion
		

		#region Helpers

		private bool Advertise()
		{
			if (this.CanBeModified)
			{
				this.Status = JobStatus.Active;
				if (this.FirstBookingAboutToStart)
					this.Status = JobStatus.PendingCoordinatorAction;
				this.UpdatedAt = DateTime.Now;
				this.LastAdvertised = DateTime.Now;
				return true;
			}
			return false;
		}

		private bool Apply(int userID)
		{
			if (this.CanAcceptApplicant && Applications.Count(c => c.UserID == userID) == 0)
			{
				Applications.Add(Candidate.CreatedByJobApplication(userID, this.ID));
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		private bool WithdrawApplication(int userID)
		{
			var applications = Applications.Where(c => c.UserID == userID);
			if (this.CanBeModified && applications.Count() == 1)
			{
				Applications.Remove(applications.First());
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		private bool AssignPrimaryWorker(int workerID)
		{
			if (this.CanAcceptApplicant && this.Applications.Count(c => c.UserID == workerID && c.Agreed) == 1)
			{
				this.PrimaryWorkerID = workerID;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		private bool DismissPrimaryWorker()
		{
			if (this.CanBeModified)
			{
				this.PrimaryWorkerID = 0;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		private bool AddSchedule(Schedule scheduleData)
		{
			if (this.CanBeModified)
			{
				var schedules = Schedules.Where(s => s.ID == scheduleData.ID);
				if (schedules.Any() || scheduleData.JobID != this.ID)
					return false;

				Schedules.Add(scheduleData);
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		private bool Reschedule(Schedule scheduleData)
		{
			if (this.CanBeModified)
			{
				var schedules = Schedules.Where(s => s.ID == scheduleData.ID);
				if (schedules.Any() && scheduleData.JobID == this.ID)
				{
					Schedules.Remove(schedules.Single(s => s.ID == scheduleData.ID));
					Schedules.Add(scheduleData);
					this.UpdatedAt = DateTime.Now;
					return true;
				}
			}
			return false;
		}

		private bool Cancel()
		{
			if (this.CanBeModified)
			{
				this.Status = JobStatus.Cancelled;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		private bool PrimaryWorkerResign()
		{
			if (this.CanBeModified)
			{
				this.PrimaryWorkerID = 0;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		private List<Schedule> GetAllActiveSchedules()
		{
			return this.Schedules.Where(s => s.EffectiveEndDate == null || s.EffectiveEndDate > DateTime.Now).ToList();
		}

		#endregion
	}
}