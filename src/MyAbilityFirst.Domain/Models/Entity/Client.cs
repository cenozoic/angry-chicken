﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cenozoic.Models;

namespace MyAbilityFirst.Domain
{
	public class Client : User
	{

		#region Properties

		public Disclaimers Disclaimers { get; set; }

		public virtual ICollection<Patient> Patients { get; set; }
		public virtual ICollection<Job> Jobs { get; set; }
		public virtual ICollection<Booking> Bookings { get; set; }

		#endregion

		#region Ctor

		protected Client()
		{
			// required by EF
			this.Disclaimers = new Disclaimers();
			this.NotificationSettings = new NotificationSettings();
			this.Patients = new List<Patient>();
			this.Jobs = new List<Job>();
			this.Shortlists = new List<Shortlist>();
		}

		public Client(string loginIdentityId) : base(loginIdentityId)
		{
			this.Disclaimers = new Disclaimers();
			this.NotificationSettings = new NotificationSettings();
			this.Patients = new List<Patient>();
			this.Jobs = new List<Job>();
			this.Shortlists = new List<Shortlist>();
		}

		#endregion

		#region Patients

		public Patient AddNewPatient(Patient patientData)
		{
			var patients = Patients.Where(p => p.ID == patientData.ID);
			if (patients.Any())
				return patients.FirstOrDefault();

			patientData.Status = UserStatus.Active;
			patientData.CreatedAt = DateTime.Now;
			patientData.UpdatedAt = DateTime.Now;
			Patients.Add(patientData);
			return patientData;
		}

		public Patient UpdateExistingPatient(Patient patientData)
		{
			var patients = Patients.Where(p => p.ID == patientData.ID && p.ClientID == patientData.ClientID);
			if (patients.Any() && patientData.ClientID == this.ID)
			{
				Patients.Remove(patients.Single(p => p.ID == patientData.ID && p.ClientID == patientData.ClientID));
				patientData.UpdatedAt = DateTime.Now;
				Patients.Add(patientData);
				return patientData;
			}
			return null;
		}

		public Patient GetExistingPatient(int patientID)
		{
			var patients = Patients.Where(p => p.ID == patientID);
			if (patients.Any())
			{
				return patients.Single(p => p.ID == patientID);
			}
			return null;
		}

		public List<Patient> GetAllPatients()
		{
			return this.Patients.ToList();
		}

		#endregion

		#region Jobs

		public Job AddJob(Job jobData)
		{
			var jobs = Jobs.Where(j => j.ID == jobData.ID);
			if (jobs.Any())
				return jobs.FirstOrDefault();

			Jobs.Add(jobData);
			return jobData;
		}

		public Job UpdateJob(Job jobData)
		{
			var jobs = Jobs.Where(j => j.ID == jobData.ID);
			if (jobs.Any() && jobData.ClientID == this.ID)
			{
				Jobs.Remove(jobs.Single(p => p.ID == jobData.ID && p.ClientID == jobData.ClientID));
				jobData.UpdatedAt = DateTime.Now;
				Jobs.Add(jobData);
				return jobData;
			}
			return null;
		}

		public Job GetJob(int jobID)
		{
			var jobs = Jobs.Where(p => p.ID == jobID);
			if (jobs.Any())
			{
				return jobs.Single(p => p.ID == jobID);
			}
			return null;
		}

		#endregion

		#region Bookings

		public Booking GetBooking(int bookingID)
		{
			var bookings = Bookings.Where(p => p.ID == bookingID);
			if (bookings.Any())
			{
				return bookings.Single(p => p.ID == bookingID);
			}
			return null;
		}

		#endregion

	}
}
