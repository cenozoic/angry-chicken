﻿namespace MyAbilityFirst.Domain
{
	public class DeadLine
	{
		public const double HoursBeforeCoordinatorEscalation = 4;
		public const double HoursBeforeBookingStartsBuffer = 4; // time before booking starts
		public const double HoursBeforeJobChangesAutomaticallyFinalize = HoursBeforeCoordinatorEscalation + 1; // 1 more hour added for finalizing changes
		public const double MinutesBeforeBookingChangesAutomaticallyFinalize = 1;
	}
}