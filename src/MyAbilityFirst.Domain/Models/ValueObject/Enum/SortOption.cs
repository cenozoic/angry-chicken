﻿namespace MyAbilityFirst.Domain
{
	public enum SortOption 
	{
		Closest = 0,
		MostRecent = 1,
		Rating = 2
	}
}
