﻿namespace MyAbilityFirst.Domain
{
	public enum JobStatus
	{
		Draft,
		Active,
		PendingCoordinatorAction,
		Expired,
		Cancelled
	}
}