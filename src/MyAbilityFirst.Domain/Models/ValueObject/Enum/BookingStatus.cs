﻿namespace MyAbilityFirst.Domain
{
	public enum BookingStatus
	{
		Booked,
		PendingCoordinatorAction,
		InProgress,
		Completed,
		Cancelled
	}
}