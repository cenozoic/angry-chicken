﻿using Cenozoic.Models;

namespace MyAbilityFirst.Domain
{
	public class BookingNotification : Notification
	{

		#region Properties

		public int BookingID { get; private set; }
		public bool AcceptedChange { get; set; }

		#endregion

		protected BookingNotification()
		{
			// Required by EF
		}

		public BookingNotification(int ownerUserID, string noticeFrom, int bookingID) : base(ownerUserID, noticeFrom)
		{
			this.BookingID = bookingID;
			this.AcceptedChange = false;
		}

		public override void SetAsRead()
		{
			base.SetAsRead();
		}

	}
}
