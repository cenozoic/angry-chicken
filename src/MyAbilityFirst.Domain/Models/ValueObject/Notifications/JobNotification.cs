﻿using Cenozoic.Models;

namespace MyAbilityFirst.Domain
{
	public class JobNotification : Notification
	{

		#region Properties

		public int JobID { get; private set; }
		public bool AcceptedChange { get; set; }

		#endregion

		protected JobNotification()
		{
			// Required by EF
		}

		public JobNotification(int ownerUserID, string noticeFrom, int jobID) : base(ownerUserID, noticeFrom)
		{
			this.JobID = jobID;
			this.AcceptedChange = false;
		}

		public override void SetAsRead()
		{
			base.SetAsRead();
		}

	}
}
