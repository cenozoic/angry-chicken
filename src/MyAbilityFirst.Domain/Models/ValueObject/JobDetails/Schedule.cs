﻿using System;
using System.Collections.Generic;
using Ical.Net;
using Ical.Net.DataTypes;
using Ical.Net.Interfaces.DataTypes;
using System.Linq;

namespace MyAbilityFirst.Domain
{
	public class Schedule
	{

		#region Properties

		public int ID { get; set; }
		public int JobID { get; set; }
		public Duration Duration { get; private set; }
		public virtual ICollection<Booking> Bookings { get; set; }
		public ScheduleType ScheduleType { get; private set; }
		public DateTime? EffectiveEndDate { get; private set; }

		#endregion

		#region Ctor

		protected Schedule()
		{
			// required by EF
		}

		public Schedule(DateTime start, DateTime end, ScheduleType scheduleType, DateTime? effectiveEndDate = null)
		{
			if (start >= end)
				throw new ArgumentOutOfRangeException("start", "The DateTime value for 'start' must be less than 'end'.");

			this.Duration = new Duration(start, end);
			this.ScheduleType = scheduleType;
			this.EffectiveEndDate = (scheduleType == ScheduleType.OneOff || scheduleType == ScheduleType.OneOffReplacement) ? end : effectiveEndDate;
			this.Bookings = new List<Booking>();
		}

		#endregion
		
		public bool Overlaps(Schedule otherSchedule, DateTime searchStart, DateTime searchEnd)
		{
			return ICalOverlaps(ConvertToICal(this), ConvertToICal(otherSchedule), searchStart, searchEnd);
		}

		public List<Duration> GetScheduledDurations(DateTime searchStart, DateTime searchEnd)
		{
			return GetICalScheduledOccurences(searchStart, searchEnd);
		}

		public DateTime GetStartDate() {
			return Duration.Start;
		}

		public DateTime? GetEndDate() {
			return EffectiveEndDate;
		}

		public TimeSpan GetStartTime() {
			return Duration.Start.TimeOfDay;
		}

		public TimeSpan GetEndTime()
		{
			return Duration.End.TimeOfDay;
		}

		#region Helpers

		private List<Duration> GetICalScheduledOccurences(DateTime searchStart, DateTime searchEnd)
		{
			var c = ConvertToICal(this);
			HashSet<Occurrence> x = c.GetOccurrences(searchStart, searchEnd);
			return x.Select(o => { Duration p = new Duration(o.Period.StartTime.Date, o.Period.EndTime.Date); return p; }).ToList();
		}

		private bool ICalOverlaps(Calendar c1, Calendar c2, DateTime searchStart, DateTime searchEnd)
		{
			c1.GetOccurrences(searchStart, searchEnd).IntersectWith(c2.GetOccurrences(searchStart, searchEnd));
			return c1.GetOccurrences(searchStart, searchEnd).Count > 0;
		}

		private Calendar ConvertToICal(Schedule schedule)
		{
			Calendar c = new Calendar();

			FrequencyType ft = FrequencyType.None;
			int interval = 1;

			switch (schedule.ScheduleType)
			{
				case ScheduleType.OneOff:
				case ScheduleType.OneOffReplacement:
					c.Events.Add(new Event
					{
						DtStart = new CalDateTime(schedule.Duration.Start),
						DtEnd = new CalDateTime(schedule.Duration.End),
					});
					return c;
				case ScheduleType.Weekly:
					ft = FrequencyType.Weekly;
					break;
				case ScheduleType.Fortnightly:
					ft = FrequencyType.Weekly;
					interval = 2; // every other week
					break;
				case ScheduleType.Monthly:
					ft = FrequencyType.Monthly;
					break;
				case ScheduleType.Yearly:
					ft = FrequencyType.Yearly;
					break;
				default:
					break;
			}

			Event e = new Event
			{
				DtStart = new CalDateTime(schedule.Duration.Start),
				DtEnd = new CalDateTime(schedule.Duration.End),
				RecurrenceRules = new List<IRecurrencePattern>(),
			};
			RecurrencePattern rp = new RecurrencePattern(ft, interval);
			e.RecurrenceRules.Add(rp);
			c.Events.Add(e);
			return c;
		}

		#endregion

	}
}