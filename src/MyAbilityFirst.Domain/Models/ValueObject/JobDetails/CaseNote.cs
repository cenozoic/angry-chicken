﻿using System;

namespace MyAbilityFirst.Domain
{
	public class CaseNote
	{

		#region Properties

		public int ID { get; set; }
		public int BookingID { get; private set; }
		public int AuthorID { get; private set; }
		public string Note { get; set; }
		public DateTime CreatedAt { get; private set; }

		#endregion

		#region Ctor

		protected CaseNote()
		{
			// Required by EF
		}

		public CaseNote(int bookingID, int authorID, string note)
		{
			this.BookingID = BookingID;
			this.AuthorID = authorID;
			this.Note = note;
			this.CreatedAt = DateTime.Now;
		}

		#endregion

	}
}