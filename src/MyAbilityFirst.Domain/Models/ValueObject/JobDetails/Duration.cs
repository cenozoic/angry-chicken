﻿using System;

namespace MyAbilityFirst.Domain
{
	public class Duration
	{
		public DateTime Start { get; set; }
		public DateTime End { get; set; }

		public Duration(DateTime start, DateTime end)
		{
			Start = start > end ? end : start;
			End = end > start ? end : start;
		}
	}
}
