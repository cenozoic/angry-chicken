﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MyAbilityFirst.Domain
{
	public class Booking
	{

		#region Properties

		public int ID { get; set; }
		public int ClientID { get; private set; }
		public int WorkerID { get; private set; }
		public Duration Duration { get; private set; }
		public int ScheduleID { get; private set; }
		public BookingStatus Status { get; private set; }
		public virtual ICollection<CaseNote> CaseNotes { get; private set; }
		public virtual ICollection<Candidate> ReplacementInvites { get; private set; }
		public virtual Rating Rating { get; private set; }
		public DateTime CreatedAt { get; private set; }
		public DateTime UpdatedAt { get; private set; }
		
		public bool CanBeModified => !(this.Status == BookingStatus.Cancelled || this.Status == BookingStatus.Completed || this.Status == BookingStatus.InProgress || this.AboutToStart);
		public bool NotYetFinalized => ( 
			this.WorkerID == 0 || this.Status == BookingStatus.PendingCoordinatorAction
		);
		public bool AboutToStart => (MinutesBeforeBookingStarts <= DeadLine.MinutesBeforeBookingChangesAutomaticallyFinalize);
		public bool IsInProgress => (this.Duration.Start < DateTime.Now && this.Duration.End > DateTime.Now);
		public double HoursBeforeBookingStarts => ((this.Duration.Start - DateTime.Now).TotalHours);
		public double MinutesBeforeBookingStarts => ((this.Duration.Start - DateTime.Now).TotalMinutes);

		#endregion

		#region Ctor

		protected Booking()
		{
			// Required by EF
			this.CaseNotes = new List<CaseNote>();
		}

		public Booking(int workerID, int scheduleID)
		{
			this.WorkerID = workerID;
			this.ScheduleID = scheduleID;
			this.Status = BookingStatus.Booked;
			this.CaseNotes = new List<CaseNote>();
			this.CreatedAt = DateTime.Now;
			this.UpdatedAt = DateTime.Now;
		}

		#endregion

		#region Client transitions

		public bool ReassignCarerByClient(int workerID, string carerReplacementName)
		{
			return ReassignCarer(workerID, carerReplacementName, this.ClientID);
		}

		public bool RescheduleByClient(Duration newDuration)
		{
			return Reschedule(newDuration, this.ClientID);
		}

		public bool InviteReplacementByClient(int replacementWorkerID)
		{
			return InviteReplacementCandidate(replacementWorkerID);
		}
		
		public bool CancelByClient()
		{
			if (this.CanBeModified)
			{
				AddCaseNote(this.ClientID, "cancelled this booking");
				this.Status = BookingStatus.Cancelled;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		#endregion


		#region Carer transitions

		public bool RejectByCarer()
		{
			if (this.CanBeModified)
			{
				AddCaseNote(this.WorkerID, "have withdrawn from this booking");
				this.WorkerID = 0;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		public bool RejectAndInviteReplacementByCarer(int newWorkerID, string carerReplacementName)
		{
			if (this.RejectByCarer())
			{
				if (InviteReplacementCandidate(newWorkerID))
				{
					AddCaseNote(this.WorkerID, "have recommended " + carerReplacementName + " as a replacement.");
					return true;
				}
			}
			return false;
		}

		public bool RescheduleByCarer(Duration newDuration)
		{
			if (this.WorkerID != 0)
			{
				return Reschedule(newDuration, this.WorkerID);
			}
			return false;
		}

		public bool AcceptInvitationByCarer(int invitedCarerID)
		{
			return UpdateInvitation(invitedCarerID, true);
		}

		public bool RejectInvitationByCarer(int invitedCarerID)
		{
			return UpdateInvitation(invitedCarerID, false);
		}
		
		#endregion


		#region Scheduler transitions

		public bool FlagForCoordinatorActionByScheduler()
		{
			if (this.CanBeModified && this.NotYetFinalized && HoursBeforeBookingStarts <= DeadLine.HoursBeforeBookingStartsBuffer)
			{
				AddCaseNote(-1, "Booking is been assigned to central coordinator for review and finalizing");
				this.Status = BookingStatus.PendingCoordinatorAction;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		public bool FinalizeByScheduler()
		{ // finalize all changes in absent of user confirmation, X minutes before booking starts
			if (this.NotYetFinalized && this.AboutToStart)
			{
				if (this.WorkerID == 0)
				{
					AddCaseNote(-1, "Booking cancelled due to no available carers");
				} else 
				{
					AddCaseNote(-1, "Booking details finalized. Booking will commence in " + MinutesBeforeBookingStarts.ToString() + "minute(s)");
				}
				this.Status = this.WorkerID != 0 ? BookingStatus.Booked : BookingStatus.Cancelled;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}
		
		public bool SetAsInProgressByScheduler()
		{
			if (this.Status == BookingStatus.Booked && this.IsInProgress)
			{
				AddCaseNote(-1, "Booking commences");
				this.Status = BookingStatus.InProgress;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		public bool SetAsCompleteByScheduler()
		{
			if (this.Status == BookingStatus.InProgress && this.Duration.End <= DateTime.Now)
			{
				AddCaseNote(-1, "Booking ends");
				this.Status = BookingStatus.Completed;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		#endregion


		#region Coordinator transitions

		public bool ReassignCarerByCoordinator(int workerID, int coordinatorID, string carerReplacementName)
		{
			return ReassignCarer(workerID, carerReplacementName, coordinatorID);
		}

		public bool RescheduleByCoordinator(Duration newDuration, int coordinatorID)
		{
			return Reschedule(newDuration, coordinatorID);
		}

		public bool InviteReplacementByCoordinator(int replacementWorkerID)
		{
			return InviteReplacementCandidate(replacementWorkerID);
		}

		#endregion


		#region Helpers

		private bool ReassignCarer(int workerID, string carerReplacementName, int changeUserID)
		{
			if (this.CanBeModified && this.ReplacementInvites.Count(c => c.UserID == workerID && c.Agreed) == 1)
			{
				AddCaseNote(changeUserID, "assigned this booking to " + carerReplacementName + "");
				this.WorkerID = workerID;
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		private bool Reschedule(Duration newDuration, int changeUserID)
		{
			if (this.CanBeModified && newDuration.Start > DateTime.Now)
			{
				this.Duration = newDuration;
				this.UpdatedAt = DateTime.Now;
				AddCaseNote(changeUserID, "rescheduled this booking to " + newDuration.Start.ToString() + " - " + newDuration.End.ToString());
				return true;
			}
			return false;
		}
		
		private bool InviteReplacementCandidate(int workerID)
		{
			if (this.CanBeModified && this.WorkerID == 0 && this.ReplacementInvites.Count(c => c.UserID == workerID) == 0)
			{
				this.ReplacementInvites.Add(Candidate.CreateByBookingInvite(workerID, this.ID));
				this.UpdatedAt = DateTime.Now;
				return true;
			}
			return false;
		}

		private bool UpdateInvitation(int invitedCarerID, bool agreed)
		{
			if (this.CanBeModified)
			{
				var invitee = ReplacementInvites.Where(c => c.UserID == invitedCarerID).FirstOrDefault();
				if (invitee != null)
				{
					ReplacementInvites.Remove(invitee);
					invitee.Agreed = agreed;
					ReplacementInvites.Add(invitee);
					return true;
				}
			}
			return false;
		}

		#endregion


		#region Case Notes

		public void AddCaseNote(int authorID, string note)
		{
			var caseNote = new CaseNote(this.ID, authorID, note);
			this.CaseNotes.Add(caseNote);
		}

		public CaseNote GetCaseNote(int caseNoteID)
		{
			return this.CaseNotes.Where(c => c.ID == caseNoteID).SingleOrDefault();
		}

		public void UpdateCaseNote(CaseNote caseNote)
		{
			var item = this.GetCaseNote(caseNote.ID);
			if (item != null)
			{
				this.CaseNotes.Remove(item);
				this.CaseNotes.Add(caseNote);
			}
		}

		public CaseNote DeleteCaseNote(int caseNoteID, int userID)
		{
			var item = this.GetCaseNotesByUserID(userID).Where(c => c.ID == caseNoteID).SingleOrDefault();
			if (item != null)
				this.CaseNotes.Remove(item);

			return item;
		}

		public List<CaseNote> GetCaseNotes()
		{
			return this.CaseNotes.ToList();
		}

		public List<CaseNote> GetCaseNotesByUserID(int userID)
		{
			return this.CaseNotes.Where(c => c.AuthorID == userID).ToList();
		}

		#endregion


		#region Rating

		public bool AddRating(Rating rating)
		{
			if (rating.BookingID == this.ID)
			{
				Rating = rating;
				return true;
			}
			return false;
		}

		public bool UpdateRating(Rating newRating)
		{
			if (newRating.BookingID == this.ID)
			{
				Rating = newRating;
				return true;
			}
			return false;
		}

		#endregion
	}
}