﻿using System;

namespace MyAbilityFirst.Domain
{
	public class Candidate
	{
		public int ID { get; set; }
		public int UserID { get; set; }
		public int JobID { get; set; }
		public int BookingID { get; set; }
		public bool Agreed { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime UpdatedAt { get; set; }

		protected Candidate()
		{
			// Required by EF
		}

		protected Candidate(int userID, int jobID, int bookingID, bool agreed)
		{
			this.UserID = userID;
			this.JobID = jobID;
			this.BookingID = bookingID;
			this.Agreed = agreed;
			this.CreatedAt = DateTime.Now;
			this.UpdatedAt = DateTime.Now;
		}

		public static Candidate CreateByJobInvite(int userID, int jobID)
		{
			return new Candidate(userID, jobID, 0, false);
		}

		public static Candidate CreatedByJobApplication(int userID, int jobID)
		{
			return new Candidate(userID, jobID, 0, true);
		}

		public static Candidate CreateByBookingInvite(int userID, int bookingID)
		{
			return new Candidate(userID, 0, bookingID, false);
		}

		public static Candidate CreatedByBookingApplication(int userID, int bookingID)
		{
			return new Candidate(userID, 0, bookingID, true);
		}

	}
}
