﻿using System.ComponentModel.DataAnnotations;

namespace MyAbilityFirst.Domain
{
	public class ForgotPasswordViewModel
	{
		[Required]
		[EmailAddress]
		[Display(Name = "Email")]
		public string Email { get; set; }
	}
}