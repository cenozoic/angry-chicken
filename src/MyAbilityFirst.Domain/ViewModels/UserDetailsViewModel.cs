﻿using System;
using System.ComponentModel.DataAnnotations;
using Cenozoic.Models;

namespace MyAbilityFirst.Domain
{
	public class UserDetailsViewModel
	{
		// User
		[Required]
		public string FirstName { get; set; }

		[Required]
		public string LastName { get; set; }

		[Required]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy}")]
		public DateTime? DoB { get; set; }

		public int GenderID { get; set; }

		[Required]
		[EmailAddress]
		public string Email { get; set; }

		public string Phone { get; set; }

		public Address Address { get; set; }
	}
}