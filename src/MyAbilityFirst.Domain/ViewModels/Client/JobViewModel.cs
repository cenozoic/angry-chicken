using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Cenozoic.Models;

namespace MyAbilityFirst.Domain
{
	public class JobViewModel
	{
		[Key]
		public int ID { get; set; }
		public int ClientID { get; set; }
		[Required(AllowEmptyStrings = false)]
		[Display(Name = "Title")]
		public string Title { get; set; }

		[Required]
		[Display(Name = "Description")]
		public string Description { get; set; }
 
		public Address Address { get; set; }

		[Display(Name = "Service Required")]
		public int ServiceID { get; set; }

		public DateTime? CreatedAt { get; set; }
		public DateTime? UpdatedAt { get; set; }

		public IEnumerable<SelectListItem> ServiceDropDownList { get; set; }
		public IEnumerable<SelectListItem> PatientDropDownList { get; set; }
	}
}